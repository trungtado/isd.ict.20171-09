import numpy as np
import unittest

class TwoLinearEquations:

    def init(self):
        pass

    def solve(self, a1, b1, c1, a2, b2, c2):
        det = np.linalg.det(np.array([[a1, b1],[a2, b2]]))
        if det == 0:
            return
            raise ValueError('Cannot solve these equations')
        det_x = np.linalg.det(np.array([[b1, c1],[b2, c2]]))
        det_y = np.linalg.det(np.array([[a1, c1],[a2, c2]]))
        return -det_x/det, det_y/det

class TwoLinearEquationsTestCase(unittest.TestCase):

    def setUp(self):
        self.solver = TwoLinearEquations()

    def tearDown(self):
        del self.solver
        self.solver = None

    def test_parameters_has_one_solution(self):
        x, y = self.solver.solve(1,2,3,4,5,6)
        self.assertAlmostEqual(x, -1, delta=1e-8)
        self.assertAlmostEqual(y, 2, delta=1e-8)

    def test_parameters_has_one_solution_float(self):
        x, y = self.solver.solve(1.5, 1.7, 3, 1.8, 1.9, 4)
        self.assertAlmostEqual(x, 5.238095238, delta=1e-8)
        self.assertAlmostEqual(y, -2.857142857, delta=1e-8)

    def test_parameters_has_infinite_solutions(self):        
        self.assertRaises(self.solver.solve(1,1,1,1,1,1))

    def test_parameters_has_no_solution(self):
        self.assertRaises(self.solver.solve(1,1,1,2,2,2))

    def test_boundary_has_no_solution(self):
        self.assertRaises(self.solver.solve(1,1.00000001,1,2,2.00000002,2))

    def test_boundary_has_solution_upper(self):
        x, y = self.solver.solve(1,1.00000001,1,2,2.00000003,2)
        self.assertAlmostEqual(x, 1, delta=1e-8)
        self.assertAlmostEqual(y, 0, delta=1e-8)

    def test_boundary_has_solution_lower(self):
        x, y = self.solver.solve(1,1.00000001,1,2,2.00000001,2)
        self.assertAlmostEqual(x, 1, delta=1e-8)
        self.assertAlmostEqual(y, 0, delta=1e-8)

if __name__ == '__main__':
    unittest.main()