import unittest


class LengthOfEdgesTriagle:
    def init(self):
        pass

    def solve(self, a, b, c):
        sum = a + b + c
        return sum

    def biggest(self, a, b, c):
        max = a
        if b > max:
            max = b
        if c > max:
            max = c
            if b > c:
                max = b
        return max

    def min(self, a, b, c):
        min = c
        if a < b and a < c:
            min = a
        elif b < c:
            min = b
        return min


class LengthOfEdgesTriagleTestCase(unittest.Testcase):
    def setUp(self):
        self.solver = lengthOfEdgesTriagle()

    def tearDown(self):
        del self.solver
        self.solver = None

    def one_of_eddes_is_negative(self):
        min = self.solver.min(-1, 2, 3)
        self.assertGreater(0, min)

    def test_is_edges_of_triagle(self):
        sum = self.solver.solve(1, 2, 3)
        max = self.solver.biggest(1, 2, 3)
        self.assertGreater(sum, max)

    def test_is_not_edges_of_triagle(self):
        sum = self.solver.solve(1, 2, 3)
        max = self.solver.biggest(1, 2, 3)
        self.assertGreater(max, sum)


if __name__ == '__main__':
    unittest.main()
