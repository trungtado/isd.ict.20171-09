import unittest
import math
class QuadraticEquation:

    def init(self):
        pass

    def solve(self, A,B,C):
        if A == 0:
            if B == 0:
                if C == 0:
                    return
                    raise ValueError('Equation has infinte solution')
                else:
                    return
                    raise ValueError('Equation has no solution')
            else:
                return -C/B
        else:
            delta = B*B - 4*A*C
            if Delta < 0:
                return
                raise ValueError('Equation has no solution')
            else if Delta == 0:
                return -B/(2*A)
            else:
                return (-B + math.sqrt(Delta))/(2*A),(-B - math.sqrt(Delta))/(2*A)

class QuadraticEquationTestCase(unittest.TestCase):

    def setUp(self):
        self.solver = QuadraticEquation()

    def tearDown(self):
        del self.solver
        self.solver = None

    def test_infinite_solution_without_A(self):
        self.assertRaises(self.solver.solve(0,0,0))

    def test_no_solution_without_A(self):
        self.assertRaises(self.solver.solve(0,0,3))
    
    def test_one_solution_without_A(self):
        x = self.solver.solve(0,2,-6)
        self.assertAlmostEqual(x,3,delta=1e-8)

    def test_no_solution_with_A(self):
        self.assertRaises(self.solver.solve(1,1,1))

    def test_one_solution_with_A(self):
        x = self.solver.solve(1,2,1)
        self.assertAlmostEqual(x,-1,delta=1e-8)

    def test_two_solution_with_A(self):
        x , y= self.solver.solve(1,3,2)
        self.assertAlmostEqual(x,y,-1,-2,delta=1e-8)

if __name__ == '__main__':
    unittest.main()