from ..models import *
from django.views import View
from django.utils.decorators import method_decorator
from django.template.response import TemplateResponse
from django.contrib.auth.decorators import login_required

@method_decorator([login_required(login_url='/login/')], name='dispatch')
class ClearCartView(View, CartSmartProxy):
    template_name = 'transaction/cart.html'

    def get(self, request):
        self.clear(request)
        context = {'success': 'Successfully clear cart'}
        return TemplateResponse(request, self.template_name, context)
    
    def post(self, request):
        self.clear(request)
        context = {'success': 'Successfully clear cart'}
        return TemplateResponse(request, self.template_name, context)

