from ..models import *
from django.views import View
from django.utils.decorators import method_decorator
from django.template.response import TemplateResponse
from django.contrib.auth.decorators import login_required


@method_decorator([login_required(login_url='/login/')], name='dispatch')
class CartView(View, CartSmartProxy):
    template_name = 'transaction/cart.html'

    def get(self, request):
        context = {}
        return TemplateResponse(request, self.template_name, context)