from ..models import *
from account.models import *
from django.views import View
from django.utils.decorators import method_decorator
from django.template.response import TemplateResponse
from django.contrib.auth.decorators import login_required

@method_decorator([login_required(login_url='/login/')], name='dispatch')
class BorrowView(View):

    template_name = 'transaction/borrow.html'

    def get(self, request):
        try:
            card = Card.objects.get(user=request.user)
        except Card.DoesNotExist:
            context = {}
            return TemplateResponse(request, self.template_name, context)

        borrowed = []
        for transaction in Transaction.objects.filter(card=card):
            borrowed.append({
                'transaction': transaction,
                'copy': transaction.copy,
                'book': transaction.copy.book,    
            })
        context = {'user': request.user, 'borrowed': borrowed}
        return TemplateResponse(request, self.template_name, context)