from ..models import *
from ..forms import *
from book.models import *
from account.models import *
from datetime import date
from django.views import View
from django.utils.decorators import method_decorator
from django.template.response import TemplateResponse
from django.contrib.auth.decorators import login_required

@method_decorator([login_required(login_url='/login/')], name='dispatch')
class CheckOutView(View, CartSmartProxy):
    '''
    Borrower can click check out to officially submit borrowing request to the system
    '''
    template_name = 'transaction/cart.html'

    def get(self, request):
        '''
        Examine list of books and copies user has added to cart. Check whether they are available to officially borrow. If any condition is not met, display error message. Otherwise save borrowing record to database and display success message

        :param request: HTTP GET request to this view, contain cart information in session
        :type request: :class:`django.http.HttpRequest`

        :returns: :class:`django.template.response.TemplateResponse`, 
          contain request, path to template file, and context

          context include success or error message
        '''
        cart = request.session['cart']

        if len(cart) == 0:
            context = {'error': 'Your cart is empty'}
            return TemplateResponse(request, self.template_name, context)

        while len(cart) > 0:
            item = cart.pop(0)
            
            if not Book.objects.filter(id=item['book_id']).exists():
                context = {'error': 'Book with id %d is not exist anymore' % item['book_id']}
                return TemplateResponse(request, self.template_name, context)
            if not Copy.objects.filter(id=item['copy_id']).exists():
                context = {'error': 'Copy with id %d is not exist anymore' % item['copy_id']}
                return TemplateResponse(request, self.template_name, context)
            copy = Copy.objects.get(id=item['copy_id'])
            if copy.type != 'A':
                context = {'error': 'Copy with id %d is not available anymore' % item['copy_id']}
                return TemplateResponse(request, self.template_name, context)
            if Transaction.objects.filter(copy=copy).exists():
                context = {'error': 'Copy with id %d is not available anymore' % item['copy_id']}
                return TemplateResponse(request, self.template_name, context)

            Transaction.objects.create(
                type='B',
                start=date.today(),
                card=Card.objects.get(user=request.user),
                copy=copy,
            )

        request.session['cart'] = cart
        context = self.get_context(request)
        context['success'] = 'Successfully check out'
        return TemplateResponse(request, self.template_name, context)
