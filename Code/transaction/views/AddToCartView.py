from ..models import *
from ..forms import *
from django.views import View
from django.utils.decorators import method_decorator
from django.template.response import TemplateResponse
from django.contrib.auth.decorators import login_required

@method_decorator([login_required(login_url='/login/')], name='dispatch')
class AddToCartView(View, CartSmartProxy):
    '''
    Borrowers may register to borrow books after browsing catalog and selecting some books to be
    borrowed. A borrower cannot borrow books if his/her borrower card is expired or he/she still
    borrowed any overdue unreturned books. The status of each copy indicates it is available,
    referenced, borrowed (not lend physically to borrowers) or lent (lend physically to borrowers).
    Only the available copy is allowed to be borrowed.
    '''
    template_name = 'transaction/cart.html'

    def get(self, request):
        '''
        Return add to cart form to borrower

        :param request: HTTP GET request to this view
        :type request: :class:`django.http.HttpRequest`

        :returns: :class:`django.template.response.TemplateResponse`, 
                  contain request, path to template file, and context

                  context include :class:`transaction.forms.AddToCartForm()`
        '''
        add_to_cart_form = AddToCartForm()
        context = {'add_to_cart_form': add_to_cart_form}
        return TemplateResponse(request, self.template_name, context)

    def post(self, request):
        '''
        Receive add to cart form data from user and check multiple condition

        :param request: HTTP POST request to this view, contain :class:`transaction.forms.AddToCartForm()` form data
        :type request: :class:`django.http.HttpRequest`

        :returns: :class:`django.template.response.TemplateResponse`, 
                  contain request, path to template file, and context

                  context include error message if any requirement is not satisfy, otherwise return an success message
        '''
        add_to_cart_form = AddToCartForm(request.POST)

        if add_to_cart_form.is_valid():
            book = add_to_cart_form.cleaned_data['book_id']

            # Stage 1:
            if not self.has_card(request.user):
                context = {'error': 'You do not have borrowing card'}
                return TemplateResponse(request, self.template_name, context)
            # Stage 2:
            if self.card_expired(request.user):
                context = {'error': 'Your card is expired'}
                return TemplateResponse(request, self.template_name, context)
            # Stage 3:
            if self.in_possession(book, request):
                context = {'error': 'You already possess this book'}
                return TemplateResponse(request, self.template_name, context)
            # Stage 4:
            if self.still_borrow_overdue_books(request.user):
                context = {'error': 'You still must return overdue books to borrow more'}
                return TemplateResponse(request, self.template_name, context)
            # Stage 5:
            if self.exceed_max_copies(request):
                context = {'error': 'Cart is full'}
                return TemplateResponse(request, self.template_name, context)
            # Okay, now save to cart
            self.save(request, book)
            context = {
                'add_to_cart_form': add_to_cart_form,
                'success': 'Successfully added %s to cart' % self.get_name(book)
            }
            return TemplateResponse(request, self.template_name, context)

        context = {'add_to_cart_form': add_to_cart_form}
        return TemplateResponse(request, self.template_name, context)

