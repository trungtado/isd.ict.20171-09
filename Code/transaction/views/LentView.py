from ..models import *
from ..forms import *
from book.models import *
from account.models import *
from datetime import date
from django.db import models
from django.views import View
from django.utils.decorators import method_decorator
from django.template.response import TemplateResponse
from django.contrib.auth.decorators import login_required, user_passes_test

@method_decorator(
    [login_required(login_url='/login/'), 
    user_passes_test(lambda u: u.groups.filter(name='Librarian').count() == 1, login_url='/denied/')],
    name='dispatch')
class LentView(View):
    '''
    Then the borrower goes to librarian to collect expected copies of books with his/her borrowing
    card. The librarian can search borrowing information and check if this is the expected borrower.
    If the librarian accept the borrower to borrow books, she/he then take the copy and lend them
    out to the borrower. The Lent Date and Expected Return Date are updated and notified to the
    borrower by the librarian.
    '''    
    template_name = 'transaction/lent.html'

    def get(self, request):
        '''
        Delete old lent information in user session, then return search borrow information form

        :param request: HTTP GET request to this view, contain cart information in session
        :type request: :class:`django.http.HttpRequest`

        :returns: :class:`django.template.response.TemplateResponse`, 
                  contain request, path to template file, and context

                  context contain :class:`transaction.form.SearchBorrowInformationForm()`
        '''
        if 'lent_username' in request.session:
            del request.session['lent_username']
        context = {
            'search_borrowing_information_form':  SearchBorrowInformationForm()}
        return TemplateResponse(request, self.template_name, context)

    def post_stage_1_show_borrowing_information(self, request):
        '''
        Verify search borrowing information submitted by librarian. If correct return list of books and copies that borrower registered to borrow, and other useful information of borrower to further assure that librarian give book to the right person.

        :param request: HTTP POST request to this view, contain :class:`transaction.forms.SearchBorrowInformationForm()` form data       
         
        :type request: :class:`django.http.HttpRequest`

        :returns: :class:`django.template.response.TemplateResponse`, 
                  contain request, path to template file, and context

                  context contain

                  * transactions: list of instances of :class:`transaction.models.Transaction()`

                  * borrow_user: instance of :class:`account.models.User()` 

                  * total: int, amount of money user need to pay in order to receive those books

        '''
        search_borrowing_information_form = SearchBorrowInformationForm(data=request.POST)

        if search_borrowing_information_form.is_valid():
            username = search_borrowing_information_form.cleaned_data['username']
            user = User.objects.get(username=username)
            card = Card.objects.get(user=user)
            transactions = Transaction.objects.filter(card=card, type='B')
            total = Transaction.objects.filter(card=card, type='B').\
                aggregate(models.Sum('copy__price'))['copy__price__sum']                
            if total:
                total = total * Setting.load().borrow_rate
            lent_form = LentForm(transactions)
            context = {
                'transactions': transactions, 'lent_form': lent_form,
                'borrow_user': user,
                'total': total,
            }
            if user.is_student:
                context['student'] = Student.objects.get(user=user)
            else:
                context['not_student'] = NotStudent.objects.get(user=user)
            request.session['lent_username'] = username
            return TemplateResponse(request, self.template_name, context)

        context = {
            'search_borrowing_information_form': search_borrowing_information_form,
        }
        return TemplateResponse(request, self.template_name, context)

    def post_stage_2_record_lent_information(self, request):
        '''
        Finally move borrow record to lent record and display success message. Also record this change to history.

        :param request: HTTP POST request to this view, in which its session contain lent information
        :type request: :class:`django.http.HttpRequest`

        :returns: :class:`django.template.response.TemplateResponse`, 
                  contain request, path to template file, and context

                  context contain success message
        '''
        user = User.objects.get(username=request.session['lent_username'])
        card = Card.objects.get(user=user)
        transactions = Transaction.objects.filter(card=card, type='B')
        lent_form = LentForm(transactions, data=request.POST)

        if lent_form.is_valid():
            for transaction_id in lent_form.cleaned_data['transactions']:
                
                transaction = Transaction.objects.get(id=transaction_id)
                transaction.delete()
                new_transaction = Transaction.objects.create(
                    type='L',
                    card=transaction.card,
                    copy=transaction.copy,
                )
                new_transaction.save()
                history = History.objects.create(
                    type='B',
                    start=transaction.start,
                    card=transaction.card,
                    copy=transaction.copy, 
                    description='''You took this copy from librarian,
                    remember to return this copy before %s
                    ''' % new_transaction.get_due(),
                )
                history.save()

            del request.session['lent_username']
            context = {
                'lent_form': lent_form,
                'success': 'Successfully lent',
            }
        else:
            context = {'lent_form': lent_form}
        return TemplateResponse(request, self.template_name, context)

    def post(self, request):
        '''
        Depend on session of request, redirect to appropriate post stage method

        :param request: HTTP POST request to this view, in which its session contain lent information
        :type request: :class:`django.http.HttpRequest`

        :returns: same as the method that this method redirected to
        '''
        if 'lent_username' in request.session:
            return self.post_stage_2_record_lent_information(request)
        return self.post_stage_1_show_borrowing_information(request)