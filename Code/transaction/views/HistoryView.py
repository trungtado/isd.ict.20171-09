from ..models import *
from ..forms import *
from account.models import *
from datetime import date
from django.views import View
from django.utils.decorators import method_decorator
from django.template.response import TemplateResponse
from django.contrib.auth.decorators import login_required


@method_decorator([login_required(login_url='/login/')], name='dispatch')
class HistoryView(View):
    template_name = 'transaction/history.html'

    def get(self, request):
        try:
            card = Card.objects.get(user=request.user) 
        except Card.DoesNotExist:
            context = {'error': 'You have to go to librian to issue a borrowing card first'}
            return TemplateResponse(request, self.template_name, context)

        records = []
        for history in History.objects.filter(card=card):
            records.append({
                'history': history,
                'card': history.copy,
                'book': history.copy.book,    
            })                                               
        context = {
            'user': request.user, 
            'records': records,
        }
        return TemplateResponse(request, self.template_name, context)
