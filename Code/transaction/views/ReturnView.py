from ..models import *
from ..forms import *
from book.models import *
from account.models import *
from datetime import date
from django.views import View
from django.utils.decorators import method_decorator
from django.template.response import TemplateResponse
from django.contrib.auth.decorators import login_required, user_passes_test

@method_decorator(
    [login_required(login_url='/login/'), 
    user_passes_test(lambda u: u.groups.filter(name='Librarian').count() == 1, login_url='/denied/')],
    name='dispatch')
class ReturnView(View):
    '''
    The borrower brings the books to be returned to the librarian. The librarian then keys in the
    borrower card number or copy numbers to search the lent books. The copy number, book name
    and borrower name are checked for confirmation. The borrower may pay compensation for
    damages/lostness or overdue return. The librarian then get the copies, put them to the book shelf
    and update the borrowing information as well as the copies status.
    '''
    template_name = 'transaction/return.html'

    def get(self, request):
        '''
        Delete old return information in user session, then return search lent information form

        :param request: HTTP GET request to this view, contain cart information in session
        :type request: :class:`django.http.HttpRequest`

        :returns: :class:`django.template.response.TemplateResponse`, 
                  contain request, path to template file, and context

                  context contain :class:`transaction.form.SearchLentInformationForm()`
        '''
        if 'lent_information' in request.session:
            del request.session['lent_information']
        context = {'search_lent_information_form': SearchLentInformationForm()}
        return TemplateResponse(request, self.template_name, context)
        
    def post_stage_1_search_lent_information(self, request):
        '''
        Verify search lent information, optional by card id or copy id and return all relevant transaction

        :param request: HTTP POST request to this view, contain search lent information
        :type request: :class:`django.http.HttpRequest`

        :returns: :class:`django.template.response.TemplateResponse`, 
                  contain request, path to template file, and context

                  context contain list of items, each item contain

                  * form: :class:`transaction.forms.VerifyLentInformationForm()`

                  * transaction: instance of :class:`transaction.models.Transaction().`

                  * transaction id: int, primary key of transaction instance

        '''
        search_lent_information_form = SearchLentInformationForm(request.POST)
        if search_lent_information_form.is_valid():
            card_id = search_lent_information_form.cleaned_data['card_id']
            copy_id = search_lent_information_form.cleaned_data['copy_id']

            if card_id:
                card = Card.objects.get(id=card_id)
                transactions = Transaction.objects.filter(card=card, type='L')
                context = {
                    'verify_lent_information_forms' : [{
                        'transaction_id': transaction.id,
                        'transaction': transaction,
                        'form': VerifyLentInformationForm(
                            initial={'transaction_id':transaction.id}
                            )} for transaction in transactions
                        ],
                }
                request.session['lent_information'] = search_lent_information_form.cleaned_data
                return TemplateResponse(request, self.template_name, context)

            if copy_id:
                copy = Copy.objects.get(id=copy_id)
                transactions = Transaction.objects.filter(copy=copy, type='L')
                context = {
                    'verify_lent_information_forms' : [{
                        'transaction_id': transaction.id,
                        'transaction': transaction,
                        'form': VerifyLentInformationForm(
                            initial={'transaction_id':transaction.id}
                            )} for transaction in transactions
                        ],
                }
                request.session['lent_information'] = search_lent_information_form.cleaned_data
                return TemplateResponse(request, self.template_name, context)
                
        context = {'search_lent_information_form': search_lent_information_form}
        return TemplateResponse(request, self.template_name, context)

    def post_stage_2_verify_lent_information(self, request):
        '''
        Check if verify lent information form is correct or not (user fullname and book title muse be correct). Then return a :class:`transaction.forms.CompensationForm()` instance. Add necessary information to session of the request.

        :param request: HTTP POST request to this view, contain :class:`transaction.forms.SearchLentInformationForm()` form data
        
        :type request: :class:`django.http.HttpRequest`

        :returns: :class:`django.template.response.TemplateResponse`, 
                  contain request, path to template file, and context

                  context may contain verify lent information form with error when this form is not correct or new  :class:`transaction.forms.CompensationForm()` instance

        '''
        verify_lent_information_form = VerifyLentInformationForm(request.POST)
        if verify_lent_information_form.is_valid():
            context = {'compensation_form': CompensationForm()}
            lent_information = request.session['lent_information']
            lent_information['transaction_id'] = verify_lent_information_form.cleaned_data['transaction_id']
            request.session['lent_information'] = lent_information
            return TemplateResponse(request, self.template_name, context)

        context = {
            'verify_lent_information_forms': [
                {'transaction_id': verify_lent_information_form.cleaned_data['transaction_id'],
                'form': verify_lent_information_form}
            ]
        }
        return TemplateResponse(request, self.template_name, context)

    def post_stage_3_enter_compensation_percentage(self, request):
        '''
        Validate compensation percentage, use that value and real copies price to calculate total compensation money. Prompt :class:`transaction.forms.CompensationVerifyForm()` for librarian to verify that borrower paid that money.

        :param request: HTTP POST request to this view, contain :class:`transaction.forms.CompensationForm()` form data
        
        :type request: :class:`django.http.HttpRequest`

        :returns: :class:`django.template.response.TemplateResponse`, 
                  contain request, path to template file, and context

                  contain compensation_verify_form and total_compensation_money
        '''
        request.POST = request.POST.copy()
        request.POST['transaction_id'] = request.session['lent_information']['transaction_id']
        compensation_form = CompensationForm(request.POST)
        if compensation_form.is_valid():
            lent_information = request.session['lent_information']
            compensation_percentage = compensation_form.cleaned_data['compensation_percentage']
            lent_information['compensation_percentage'] = compensation_percentage
            transaction = Transaction.objects.get(id=request.session['lent_information']['transaction_id'])
            total_compensation_money = compensation_percentage * transaction.copy.price / 100.0
            lent_information['total_compensation_money'] = total_compensation_money
            request.session['lent_information'] = lent_information
            context = {
                'compensation_verify_form': CompensationVerifyForm(), 
                'total_compensation_money': total_compensation_money
            }
            return TemplateResponse(request, self.template_name, context)

        context = {'compensation_form': compensation_form}
        return TemplateResponse(request, self.template_name, context)

    def post_stage_4_verify(self, request):
        '''
        Finally delete lent record and display success message. Also record this change to history.

        :param request: HTTP POST request to this view, in which its session contain lent information
        :type request: :class:`django.http.HttpRequest`

        :returns: :class:`django.template.response.TemplateResponse`, 
                  contain request, path to template file, and context

                  context contain success message
        '''
        compensation_verify_form = CompensationVerifyForm(request.POST)

        if compensation_verify_form.is_valid():
            transaction = Transaction.objects.get(id=request.session['lent_information']['transaction_id'])
            history = History.objects.create(
                type='L',
                start=transaction.start,
                card=transaction.card,
                copy=transaction.copy,
                description='returned to library with compensation %d VND' % 
                    request.session['lent_information']['total_compensation_money']
            )
            history.save()
            transaction.delete()
            context = {
                'success': 'Return book success'
            }
            del request.session['lent_information']
            return TemplateResponse(request, self.template_name, context)
        context = {'compensation_verify_form': compensation_verify_form}
        return TemplateResponse(request, self.template_name, context)

    def post(self, request):
        '''
        Depend on session of request, redirect to appropriate post stage method

        :param request: HTTP POST request to this view, in which its session contain lent information
        :type request: :class:`django.http.HttpRequest`

        :returns: same as the method that this method redirected to
        '''
        if 'lent_information' not in request.session:
            return self.post_stage_1_search_lent_information(request)
        if 'transaction_id' not in request.session['lent_information']:
            return self.post_stage_2_verify_lent_information(request)
        if 'compensation_percentage' not in request.session['lent_information']:
            return self.post_stage_3_enter_compensation_percentage(request)
        return self.post_stage_4_verify(request)
