from book.models import *

def get_cart(request):
    if 'cart' not in request.session:
        request.session['cart'] = []
    cart = request.session['cart']
    carts = Copy.objects.filter(id__in=[item['copy_id'] for item in cart])
    carts_info = []
    for copy in carts:
        carts_info.append({
            'title': copy.book.title,
            'authors': copy.book.authors,
            'cover': copy.book.cover,
            'id': copy.id,
        })

    return {
        'carts': carts_info,
    }
