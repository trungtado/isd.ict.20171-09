from ..models import *
from django import forms
from django.core.exceptions import ValidationError

class CompensationForm(forms.Form):
    compensation_percentage = forms.IntegerField(initial=0)

    def clean_compensation_percentage(self):
        compensation_percentage = self.cleaned_data['compensation_percentage']
        if compensation_percentage < 0:
            raise ValidationError('Compensation percentage cannot be negative')
        if compensation_percentage > 100:
            raise ValidationError('Compensation percentage cannot be greater than 100')
        return compensation_percentage
