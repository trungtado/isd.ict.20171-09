from ..models import *
from django import forms
from django.core.exceptions import ValidationError

class LentForm(forms.Form):

    def __init__(self, transactions, *args, **kwargs):
        super(LentForm, self).__init__(*args, **kwargs)
        CHOICES = tuple([(
            transaction.id, 
            transaction.copy.book.title + 
            '(copy sequential number: ' + str(transaction.copy.sequential_number) + 
            ')(price: ' + str(transaction.copy.price) + ' VND)' ) for transaction in transactions])
        self.fields['transactions'] = forms.TypedMultipleChoiceField(choices=CHOICES, widget=forms.CheckboxSelectMultiple)
        self.fields['accept'] = forms.BooleanField(
            help_text='Only check this if you can guarantee user pay enough money and user information is correct')

    def clean_accept(self):
        paid = self.cleaned_data['accept']
        if not paid:
            raise ValidationError('You did not accept this transaction')
        return paid