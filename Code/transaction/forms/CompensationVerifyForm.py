from ..models import *
from django import forms
from django.core.exceptions import ValidationError

class CompensationVerifyForm(forms.Form):

    paid = forms.BooleanField()

    def clean_paid(self):
        paid = self.cleaned_data['paid']
        if not paid:
            raise ValidationError('Borrower must paid compensation money')
        return paid
