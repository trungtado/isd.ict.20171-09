from ..models import *
from account.models import *
from django import forms
from django.core.exceptions import ValidationError

class SearchBorrowInformationForm(forms.Form):

    username = forms.CharField()

    def clean_username(self):
        username = self.cleaned_data['username']
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            raise ValidationError('username does not exist')
        else:
            try:
                card = Card.objects.get(user=user)
            except Card.DoesNotExist:
                raise ValidationError('user does not have borrowing card')
            else:
                if not card.is_activated:
                    raise ValidationError('user has an inactive borrowing card')
                return username