from ..models import *
from django import forms
from book.models import *
from account.models import *
from django.core.exceptions import ValidationError

class SearchLentInformationForm(forms.Form):

    card_id = forms.IntegerField(required=False)
    copy_id = forms.IntegerField(required=False)

    def clean_card_id(self):
        card_id = self.cleaned_data['card_id']
        if card_id and not Card.objects.filter(id=card_id).exists():
            raise ValidationError('Card does not exist')
        return card_id

    def clean_copy_id(self):
        copy_id = self.cleaned_data['copy_id']
        if copy_id and not Copy.objects.filter(id=copy_id).exists():
            raise ValidationError('Copy does not exist')
        return copy_id