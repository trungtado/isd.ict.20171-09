from ..models import *
from django import forms
from django.core.exceptions import ValidationError

class VerifyLentInformationForm(forms.Form):

    transaction_id = forms.IntegerField(widget=forms.HiddenInput(), required=False)
    book_title = forms.CharField()
    borrower_first_name = forms.CharField()
    borrower_last_name = forms.CharField()

    def clean_book_title(self):
        transaction_id = self.cleaned_data['transaction_id']
        transaction = Transaction.objects.get(id=transaction_id)
        book_title = self.cleaned_data['book_title']
        if transaction.copy.book.title.lower() != book_title.lower():
            raise ValidationError('Book title not match')
        return book_title

    def clean_borrower_first_name(self):
        transaction_id = self.cleaned_data['transaction_id']
        transaction = Transaction.objects.get(id=transaction_id)
        borrower_first_name = self.cleaned_data['borrower_first_name']
        if transaction.card.user.first_name != borrower_first_name:
            raise ValidationError('Borrower first name do not match')
        return borrower_first_name

    def clean_borrower_last_name(self):
        transaction_id = self.cleaned_data['transaction_id']
        transaction = Transaction.objects.get(id=transaction_id)
        borrower_last_name = self.cleaned_data['borrower_last_name']
        if transaction.card.user.last_name != borrower_last_name:
            raise ValidationError('Borrower last name do not match')
        return borrower_last_name
