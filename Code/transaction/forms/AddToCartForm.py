from ..models import *
from book.models import *
from django import forms
from django.core.exceptions import ValidationError

class AddToCartForm(forms.Form):

    book_id = forms.IntegerField(widget=forms.HiddenInput(), required=False)

    def clean_book_id(self):
        book_id = self.cleaned_data['book_id']
        try:
            book = Book.objects.get(id=book_id)
        except Book.DoesNotExist:
            raise ValidationError('Book is not existed')
        else:
            for copy in Copy.objects.filter(book=book, type='A'):
                if not Transaction.objects.filter(copy=copy).exists():
                    return {'book_id': book_id, 'copy_id': copy.id}
            raise ValidationError('Book is not avalable')