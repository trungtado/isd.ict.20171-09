from ..models import *
from book.models import *
from account.models import *
from datetime import date

# An item in cart contain
# {'copy_id': 1, 'book_id': 169}

class CartSmartProxy(object):

    def get_context(self, request):
        cart = request.session['cart']
        books = Book.objects.filter(id__in=[item['book_id'] for item in cart])
        context = {
            'carts': books
        }
        return context

    def in_possession(self, book, request):
        self.make_sure_cart_in_session(request)
        card = Card.objects.get(user=request.user)
        for transaction in Transaction.objects.filter(card=card):
            if transaction.copy.book.id == book['book_id']:
                return True
        for item in request.session['cart']:
            if item == book:
                return True
        return False

    def has_card(self, user):
        return Card.objects.filter(user=user).exists()

    def card_expired(self, user):
        card = Card.objects.get(user=user)
        if date.today() > card.expired_date:
            return True
        return False

    def exceed_max_copies(self, request):
        card = Card.objects.get(user=request.user)
        if Transaction.objects.filter(card=card).count() + len(self.get_cart(request)) \
            >= Setting.load().maximum_book_borrow:
            return True
        return False

    def still_borrow_overdue_books(self, user): 
        card = Card.objects.get(user=user)
        for transaction in Transaction.objects.filter(card=card, type='L'):
            if transaction.get_due() < date.today():
                return True
        return False

    def make_sure_cart_in_session(self, request):
        if 'cart' not in request.session:
            request.session['cart'] = []

    def clear(self, request):
        request.session['cart'] = []
        
    def save(self, request, book):
        cart = request.session['cart']
        cart.append(book)
        request.session['cart'] = cart

    def get_name(self, book):
        return Book.objects.get(id=book['book_id']).title

    def get_cart(self, request):    
        if 'cart' not in request.session:
            request.session['cart'] = []
        cart = request.session['cart']
        carts = Book.objects.filter(id__in=[item['book_id'] for item in cart])
        return carts