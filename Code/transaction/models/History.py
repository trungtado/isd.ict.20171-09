from account.models import *
from book.models import *
from django.db import models

TYPES = (('B', 'Borrow'), ('L', 'Lent'))

class History(models.Model):

    type = models.CharField(max_length=2, choices=TYPES)
    start = models.DateField()
    end = models.DateField(auto_now=True, null=True)
    card = models.ForeignKey(Card)
    copy = models.ForeignKey(Copy)
    description = models.TextField()