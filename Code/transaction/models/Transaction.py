from account.models import *
from book.models import *
from django.db import models
from django.core.exceptions import ValidationError
import datetime

TYPES = (('B', 'Borrow'), ('L', 'Lent'))

class Transaction(models.Model):

    type = models.CharField(max_length=2, choices=TYPES)
    start = models.DateField(auto_now=True)
    card = models.ForeignKey(Card)
    copy = models.OneToOneField(Copy, unique=True)

    def get_due(self):
        setting = Setting.load()
        if self.type == 'B':
            return self.start + datetime.timedelta(days=setting.borrow_expire)
        elif self.type == 'L':
            return self.start + datetime.timedelta(days=setting.lent_due)

    def clean_card(self):
        setting = Setting.load()
        if self.objects.filter(card=self.card).count() >= setting.maximum_book_borrow:
            raise ValidationError('Exceed maximum number of copies per card')
        return self.card