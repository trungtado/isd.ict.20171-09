from django_cron import CronJobBase, Schedule
from config import settings
from datetime import date, datetime
from ..models import *
import logging
logging.config.dictConfig(settings.LOGGING)

class LentDueCron(CronJobBase):
    RUN_EVERY_MINS = 1

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'transaction.crons.LentDueCron'
    logger = logging.getLogger('transaction.crons.LentDueCron')

    def do(self):
        # for transaction in Transaction.objects.filter(type='L', start=date.today()):
        for transaction in Transaction.objects.filter(
            type=':',
            start__lt=date.today()-datetime.timedelta(days=setting.len_due):

            if not History.objects.filter(
                type='L', 
                start=transaction.start, 
                card=transaction.card,
                copy=transaction.copy
                ).exists():
            
                msg = 'notify borrower due to lent due, lent at: %s, due date: %s ' % (transaction.start, transaction.get_due())
                self.logger.info(msg)
                history = History.objects.create(
                    type='L',
                    start=transaction.start,
                    card=transaction.card,
                    copy=transaction.copy, 
                    description=msg,
                )
                history.save()
