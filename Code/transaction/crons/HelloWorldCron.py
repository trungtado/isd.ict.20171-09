from django_cron import CronJobBase, Schedule
from config import settings
import logging

logging.config.dictConfig(settings.LOGGING)

class HelloWorldCron(CronJobBase):
    RUN_EVERY_MINS = 1

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'transaction.crons.HelloWorldCron'
    logger = logging.getLogger(__name__)

    def do(self):
        self.logger.info('hello world')