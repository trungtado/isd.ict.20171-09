from django_cron import CronJobBase, Schedule
from config import settings
from datetime import date, datetime
from ..models import *
import logging
logging.config.dictConfig(settings.LOGGING)

class BorrowExpireCron(CronJobBase):
    RUN_EVERY_MINS = 1

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'transaction.crons.BorrowExpireCron'
    logger = logging.getLogger(__name__)

    def do(self):
        # for transaction in Transaction.objects.filter(type='B', start=date.today()):
        for transaction in Transaction.objects.filter(
            type='B',
            start__lt=date.today()-datetime.timedelta(days=setting.borrow_expire)):

            msg = 'remove borrow due to expired, borrowed at: %s, expired at: %s' % (transaction.start, transaction.get_due())
            self.logger.info(msg)
            history = History.objects.create(
                type='B',
                start=transaction.start,
                card=transaction.card,
                copy=transaction.copy, 
                description=msg,
            )
            history.save()
            transaction.delete()
