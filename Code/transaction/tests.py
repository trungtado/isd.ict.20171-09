# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.test import TestCase
from django.test import Client
from django.core.urlresolvers import reverse
from account.models import *
from book.models import *
from .models import *
from datetime import date
from django.contrib.auth.models import Group
from pprint import pprint
from .forms import *
import random

ROUTE = 'transaction:return'

class ReturnViewTestCase(TestCase):
    '''
    Test cases developed by Le Thanh - 201444075 - 01

    Because internal structure of each method in ReturnView
    is quite simple so I cannot develop whitebox testing

    Black box testing are the following
    '''
    def setUp(self):
        self.client = Client()
        self.student_borrower = User.objects.create(
            id=1337,
            is_superuser=0,
            first_name='Queen',
            last_name='Oliver',
            email='oliver_queen@gmail.com',
            is_staff=False,
            is_active=True,
            username='green_arrow',
            password='green_arrow',
            is_student=True,
            gender='M',
            contact='Star City',
        )
        self.student_borrower.set_password('green_arrow')
        self.student_borrower.save()
        self.student = Student.objects.create(
            user=self.student_borrower,
            student_id='20141337',
            study_period=2019
        )
        self.student.save()
        card = Card.objects.create(
            user=self.student_borrower,
            expired_date=date.today(),
        )

        self.librarian = User.objects.create(
            id=1338,
            is_superuser=0,
            first_name='Barry',
            last_name='Allen',
            email='barry_allen@gmail.com',
            is_staff=False,
            is_active=True,
            username='the_flash',
            password='the_flash',
            is_student=True,
            gender='M',
            contact='Central City',
        )
        self.librarian.set_password('the_flash')
        self.librarian.save()
        librarian_group, _ = Group.objects.get_or_create(name='Librarian')
        librarian_group.user_set.add(self.librarian)

        classification = Classification.objects.create(label='A', description='Scientific')
        subclassification = Subclassification.objects.create(classification=classification, label='A', description='Mathematical')
        book = Book.objects.create(
            sequential_number=1,
            subclassification=subclassification,
            title='Linear Algebra',
            publisher='Springer',
            authors='Dan Boneh',
            isbn='92399419392139',
            description='Matrix, stuff like that',
            cover=''
        )
        copy = Copy.objects.create(
            type='A',
            sequential_number=1,
            book=book,
            price=100000,
        )
        self.transaction = Transaction.objects.create(
            type='L',
            card=card,
            copy=copy,
        )

    def test_get_permission_guest(self):
        '''guest do not have permission for this view'''
        response = self.client.get(reverse(ROUTE))
        self.assertEqual(response.status_code, 302)

    def test_get_permission_borrower(self):
        '''borrower do not have permission for this view'''
        self.client.force_login(self.student_borrower)
        response = self.client.get(reverse(ROUTE))
        self.assertEqual(response.status_code, 302)

    def test_get_permission_librarian(self):
        '''librarian must has permission for this view'''
        self.client.force_login(self.librarian)
        response = self.client.get(reverse(ROUTE))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('search_lent_information_form' in response.context)
        self.assertIs(type(response.context['search_lent_information_form']), SearchLentInformationForm)

    def test_get_reset_session(self):
        '''lent information in session must be delete'''
        self.client.force_login(self.librarian)
        '''test when session has info'''
        self.client.session['lent_information'] = {'lent_username':'adasdas'}
        response = self.client.get(reverse(ROUTE))
        self.assertFalse('lent_information' in self.client.session.keys())

        '''pass test above then'''
        '''test when session do not have info'''
        response = self.client.get(reverse(ROUTE))
        self.assertFalse('lent_information' in self.client.session.keys())

    def test_post_stage_1_card_id_incorrect(self):
        '''form must return error'''
        self.client.force_login(self.librarian)
        response = self.client.post(reverse(ROUTE), data={'card_id':-1})
        self.assertTrue('search_lent_information_form' in response.context)
        self.assertTrue(response.context['search_lent_information_form'].errors['card_id'])

    def test_post_stage_1_copy_incorrect(self):
        '''form must return error'''
        self.client.force_login(self.librarian)
        response = self.client.post(reverse(ROUTE), data={'copy_id':-1})
        self.assertTrue('search_lent_information_form' in response.context)
        self.assertTrue(response.context['search_lent_information_form'].errors['copy_id'])

    def test_post_stage_1_card_id_correct(self):
        '''must return list all verify forms'''
        self.client.force_login(self.librarian)
        response = self.client.post(reverse(ROUTE), data={'card_id':1})
        self.assertTrue('verify_lent_information_forms' in response.context)

    def test_post_stage_1_copy_id_correct(self):
        '''must return list all verify forms'''
        self.client.force_login(self.librarian)
        response = self.client.post(reverse(ROUTE), data={'copy_id':1})
        self.assertTrue('verify_lent_information_forms' in response.context)

    '''
    0_0_0 mean title incorrect, first_name incorrect and last_name incorrect
    '''
    def test_post_stage_2_0_0_0(self):
        '''must return error in corresponding place'''
        self.client.force_login(self.librarian)
        response = self.client.post(reverse(ROUTE), data={'copy_id':1})
        response = self.client.post(
            reverse(ROUTE), 
            data={
                'book_title':'a',
                'borrower_first_name':'a',
                'borrower_last_name':'a',
                'transaction_id':self.transaction.id,
            }
        )
        self.assertTrue('book_title' in response.context['verify_lent_information_forms'][0]['form'].errors.keys())
        self.assertTrue('borrower_first_name' in response.context['verify_lent_information_forms'][0]['form'].errors.keys())
        self.assertTrue('borrower_last_name' in response.context['verify_lent_information_forms'][0]['form'].errors.keys())

    def test_post_stage_2_0_0_1(self):
        '''must return error in corresponding place'''
        self.client.force_login(self.librarian)
        response = self.client.post(reverse(ROUTE), data={'copy_id':1})
        response = self.client.post(
            reverse(ROUTE), 
            data={
                'book_title':'a',
                'borrower_first_name':'a',
                'borrower_last_name':self.student_borrower.last_name,
                'transaction_id':self.transaction.id,
            }
        )
        self.assertTrue('book_title' in response.context['verify_lent_information_forms'][0]['form'].errors.keys())
        self.assertTrue('borrower_first_name' in response.context['verify_lent_information_forms'][0]['form'].errors.keys())
        self.assertFalse('borrower_last_name' in response.context['verify_lent_information_forms'][0]['form'].errors.keys())

    def test_post_stage_2_0_1_0(self):
        '''must return error in corresponding place'''
        self.client.force_login(self.librarian)
        response = self.client.post(reverse(ROUTE), data={'copy_id':1})
        response = self.client.post(
            reverse(ROUTE), 
            data={
                'book_title':'a',
                'borrower_first_name':self.student_borrower.first_name,
                'borrower_last_name':'a',
                'transaction_id':self.transaction.id,
            }
        )
        self.assertTrue('book_title' in response.context['verify_lent_information_forms'][0]['form'].errors.keys())
        self.assertFalse('borrower_first_name' in response.context['verify_lent_information_forms'][0]['form'].errors.keys())
        self.assertTrue('borrower_last_name' in response.context['verify_lent_information_forms'][0]['form'].errors.keys())

    def test_post_stage_2_0_1_1(self):
        '''must return error in corresponding place'''
        self.client.force_login(self.librarian)
        response = self.client.post(reverse(ROUTE), data={'copy_id':1})
        response = self.client.post(
            reverse(ROUTE), 
            data={
                'book_title':'a',
                'borrower_first_name':self.student_borrower.first_name,
                'borrower_last_name':self.student_borrower.last_name,
                'transaction_id':self.transaction.id,
            }
        )
        self.assertTrue('book_title' in response.context['verify_lent_information_forms'][0]['form'].errors.keys())
        self.assertFalse('borrower_first_name' in response.context['verify_lent_information_forms'][0]['form'].errors.keys())
        self.assertFalse('borrower_last_name' in response.context['verify_lent_information_forms'][0]['form'].errors.keys())

    def test_post_stage_2_1_0_0(self):
        '''must return error in corresponding place'''
        self.client.force_login(self.librarian)
        response = self.client.post(reverse(ROUTE), data={'copy_id':1})
        response = self.client.post(
            reverse(ROUTE), 
            data={
                'book_title':self.transaction.copy.book.title,
                'borrower_first_name':'a',
                'borrower_last_name':'a',
                'transaction_id':self.transaction.id,
            }
        )
        self.assertFalse('book_title' in response.context['verify_lent_information_forms'][0]['form'].errors.keys())
        self.assertTrue('borrower_first_name' in response.context['verify_lent_information_forms'][0]['form'].errors.keys())
        self.assertTrue('borrower_last_name' in response.context['verify_lent_information_forms'][0]['form'].errors.keys())

    def test_post_stage_2_1_0_1(self):
        '''must return error in corresponding place'''
        self.client.force_login(self.librarian)
        response = self.client.post(reverse(ROUTE), data={'copy_id':1})
        response = self.client.post(
            reverse(ROUTE), 
            data={
                'book_title':self.transaction.copy.book.title,
                'borrower_first_name':'a',
                'borrower_last_name':self.student_borrower.last_name,
                'transaction_id':self.transaction.id,
            }
        )
        self.assertFalse('book_title' in response.context['verify_lent_information_forms'][0]['form'].errors.keys())
        self.assertTrue('borrower_first_name' in response.context['verify_lent_information_forms'][0]['form'].errors.keys())
        self.assertFalse('borrower_last_name' in response.context['verify_lent_information_forms'][0]['form'].errors.keys())

    def test_post_stage_2_1_1_0(self):
        '''must return error in corresponding place'''
        self.client.force_login(self.librarian)
        response = self.client.post(reverse(ROUTE), data={'copy_id':1})
        response = self.client.post(
            reverse(ROUTE), 
            data={
                'book_title':self.transaction.copy.book.title,
                'borrower_first_name':self.student_borrower.first_name,
                'borrower_last_name':'a',
                'transaction_id':self.transaction.id,
            }
        )
        self.assertFalse('book_title' in response.context['verify_lent_information_forms'][0]['form'].errors.keys())
        self.assertFalse('borrower_first_name' in response.context['verify_lent_information_forms'][0]['form'].errors.keys())
        self.assertTrue('borrower_last_name' in response.context['verify_lent_information_forms'][0]['form'].errors.keys())

    def test_post_stage_2_1_1_1(self):
        '''correct the must return compensation form'''
        self.client.force_login(self.librarian)
        response = self.client.post(reverse(ROUTE), data={'copy_id':1})
        response = self.client.post(
            reverse(ROUTE), 
            data={
                'book_title':self.transaction.copy.book.title,
                'borrower_first_name':self.student_borrower.first_name,
                'borrower_last_name':self.student_borrower.last_name,
                'transaction_id':self.transaction.id,
            }
        )
        self.assertTrue('compensation_form' in response.context.keys())

    def test_post_stage_3_4_percentage_incorrect(self):
        '''return error'''
        self.client.force_login(self.librarian)
        response = self.client.post(reverse(ROUTE), data={'copy_id':1})
        response = self.client.post(
            reverse(ROUTE), 
            data={
                'book_title':self.transaction.copy.book.title,
                'borrower_first_name':self.student_borrower.first_name,
                'borrower_last_name':self.student_borrower.last_name,
                'transaction_id':self.transaction.id,
            }
        )
        response = self.client.post(
            reverse(ROUTE),
            data={
                'compensation_percentage': -1
            }
        )
        self.assertTrue('compensation_form' in response.context.keys())
        self.assertTrue('compensation_percentage' in response.context['compensation_form'].errors.keys())

        response = self.client.post(
            reverse(ROUTE),
            data={
                'compensation_percentage': 1000
            }
        )
        self.assertTrue('compensation_form' in response.context.keys())
        self.assertTrue('compensation_percentage' in response.context['compensation_form'].errors.keys())

    def test_post_stage_3_4_percentage_correct(self):
        '''return valid total amount of money'''
        self.client.force_login(self.librarian)
        response = self.client.post(reverse(ROUTE), data={'copy_id':1})
        response = self.client.post(
            reverse(ROUTE), 
            data={
                'book_title':self.transaction.copy.book.title,
                'borrower_first_name':self.student_borrower.first_name,
                'borrower_last_name':self.student_borrower.last_name,
                'transaction_id':self.transaction.id,
            }
        )
        percentage = random.randint(10, 90)
        response = self.client.post(
            reverse(ROUTE),
            data={
                'compensation_percentage': percentage
            }
        )
        self.assertEqual(
            response.context['total_compensation_money'], 
            self.transaction.copy.price * percentage / 100.0
        )
ROUTE_1 = 'transaction:lent'

class LentViewTestCase(TestCase):
    '''
    Test cases developed by Hoang Son -  - 03

    Because internal structure of each method in this view
    is quite simple so I cannot develop whitebox testing

    Black box testing are the following
    '''
    def setUp(self):
        self.client = Client()
        self.student_borrower = User.objects.create(
            id=1337,
            is_superuser=0,
            first_name='Queen',
            last_name='Oliver',
            email='oliver_queen@gmail.com',
            is_staff=False,
            is_active=True,
            username='green_arrow',
            password='green_arrow',
            is_student=True,
            gender='M',
            contact='Star City',
        )
        self.student_borrower.set_password('green_arrow')
        self.student_borrower.save()
        self.student = Student.objects.create(
            user=self.student_borrower,
            student_id='20141337',
            study_period=2019
        )
        self.student.save()
        self.librarian = User.objects.create(
            id=1338,
            is_superuser=0,
            first_name='Barry',
            last_name='Allen',
            email='barry_allen@gmail.com',
            is_staff=False,
            is_active=True,
            username='the_flash',
            password='the_flash',
            is_student=True,
            gender='M',
            contact='Central City',
        )
        self.librarian.set_password('the_flash')
        self.librarian.save()
        librarian_group, _ = Group.objects.get_or_create(name='Librarian')
        librarian_group.user_set.add(self.librarian)

    def test_get_permission_guest(self):
        '''guest do not have permission for this view'''
        response = self.client.get(reverse(ROUTE_1))
        self.assertEqual(response.status_code, 302)

    def test_get_permission_borrower(self):
        '''borrower do not have permission for this view'''
        self.client.force_login(self.student_borrower)
        response = self.client.get(reverse(ROUTE_1))
        self.assertEqual(response.status_code, 302)

    def test_get_permission_librarian(self):
        '''librarian must has permission for this view'''
        self.client.force_login(self.librarian)
        response = self.client.get(reverse(ROUTE))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('search_lent_information_form' in response.context)
        self.assertIs(type(response.context['search_lent_information_form']), SearchLentInformationForm)


ROUTE_2 = 'transaction:add-to-cart'

class AddToCartViewTestCase(TestCase):
    '''
    Test cases developed by Hoang Son -  - 03

    Because internal structure of each method in this view
    is quite simple so I cannot develop whitebox testing

    Black box testing are the following
    '''
    def setUp(self):
        self.client = Client()
        self.student_borrower = User.objects.create(
            id=1337,
            is_superuser=0,
            first_name='Queen',
            last_name='Oliver',
            email='oliver_queen@gmail.com',
            is_staff=False,
            is_active=True,
            username='green_arrow',
            password='green_arrow',
            is_student=True,
            gender='M',
            contact='Star City',
        )
        self.student_borrower.set_password('green_arrow')
        self.student_borrower.save()
        self.student = Student.objects.create(
            user=self.student_borrower,
            student_id='20141337',
            study_period=2019
        )
        self.student.save()

    def test_get_permission_guest(self):
        '''guest do not have permission for this view'''
        response = self.client.get(reverse(ROUTE_2))
        self.assertEqual(response.status_code, 302)

    def test_get_permission_borrower(self):
        '''borrower has permission for this view'''
        self.client.force_login(self.student_borrower)
        response = self.client.get(reverse(ROUTE_2))
        self.assertEqual(response.status_code, 200)

ROUTE_3 = 'transaction:check-out'

class CheckOutViewTestCase(TestCase):
    '''
    Test cases developed by Hoang Son -  - 03

    Because internal structure of each method in this view
    is quite simple so I cannot develop whitebox testing

    Black box testing are the following
    '''
    def setUp(self):
        self.client = Client()
        self.student_borrower = User.objects.create(
            id=1337,
            is_superuser=0,
            first_name='Queen',
            last_name='Oliver',
            email='oliver_queen@gmail.com',
            is_staff=False,
            is_active=True,
            username='green_arrow',
            password='green_arrow',
            is_student=True,
            gender='M',
            contact='Star City',
        )
        self.student_borrower.set_password('green_arrow')
        self.student_borrower.save()
        self.student = Student.objects.create(
            user=self.student_borrower,
            student_id='20141337',
            study_period=2019
        )
        self.student.save()

    def test_get_permission_guest(self):
        '''guest do not have permission for this view'''
        self.client.session['cart'] = []
        response = self.client.get(reverse(ROUTE_3))
        self.assertEqual(response.status_code, 302)

    def test_get_permission_borrower(self):
        '''borrower has permission for this view'''
        self.client.force_login(self.student_borrower)
        self.client.session['cart'] = []
        response = self.client.get(reverse(ROUTE_3))
        self.assertEqual(response.status_code, 200)