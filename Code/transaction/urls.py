from django.conf.urls import url
from django.contrib.auth.views import logout
from .views import *

urlpatterns = [
    url(r'^cart/$', CartView.as_view(), name='cart'),
    url(r'^clear-cart/$', ClearCartView.as_view(), name='clear-cart'),
    url(r'^add-to-cart/$', AddToCartView.as_view(), name='add-to-cart'),
    url(r'^check-out/$', CheckOutView.as_view(), name='check-out'),
    url(r'^borrow/$', BorrowView.as_view(), name='borrow'),
    url(r'^lent/$', LentView.as_view(), name='lent'),
    url(r'^history/$', HistoryView.as_view(), name='history'),
    url(r'^return/$', ReturnView.as_view(), name='return'),
]

