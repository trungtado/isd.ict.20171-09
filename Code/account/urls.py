from django.conf.urls import url
from django.contrib.auth.views import logout
from .views import *

urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^register/$', RegisterView.as_view(), name='register'),
    url(r'^login/$', LoginView.as_view(), name='login'),
    url(r'^logout/$', logout, {'next_page': '/'}, name='logout'),
    url(r'^register/student/$', StudentRegisterView.as_view()),
    url(r'^register/not-student/$', NotStudentRegisterView.as_view()),
    url(r'^issue-borrowing-card/$', IssueBorrowingCardView.as_view(), name='issue-borrowing-card'),
    url(r'^activate-borrowing-card/$', ActivateBorrowingCardView.as_view(), name='activate-borrowing-card'),
    url(r'^denied/$', DeniedView.as_view(), name='denied'),
]
