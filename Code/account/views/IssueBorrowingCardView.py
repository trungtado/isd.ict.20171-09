from ..forms import *
from ..models import *
from django.views import View
from django.utils.decorators import method_decorator
from django.template.response import TemplateResponse
from .BorrowingCardSmartProxy import BorrowingCardSmartProxy
from django.contrib.auth.decorators import login_required, user_passes_test

@method_decorator(
    [login_required(login_url='/login/'), 
    user_passes_test(lambda u: u.groups.filter(name='Librarian').count() == 1, login_url='/denied/')], 
    name='dispatch')
class IssueBorrowingCardView(View, BorrowingCardSmartProxy):
    '''
    After the registration, the guest must go to the library to be issued borrowing cards. If the guest
    is not a student in the university, he/she need to submit a deposit. If the guest is a student,
    he/she need to show his/her student card corresponding to his/her registered information. The
    librarian then issues a borrower card with a Borrower Number (sequential number), Expired
    Date with an Activated Code to the guest.
    '''
    template_name = 'account/issue-borrowing-card.html'

    def get(self, request):
        '''
        :param request: HTTP GET request to this view
        :type request: :class:`django.http.HttpRequest`
        :returns: :class:`django.template.response.TemplateResponse`, 
                  contain request, path to template file, and context
                  (context include :class:`account.forms.UserSearchForm()` to search for user
                  to be issue borrowing card)
        '''
        if 'issue_borrowing_card_user_info' in request.session:
            del request.session['issue_borrowing_card_user_info']
        context = {'user_search_form': UserSearchForm()}
        return TemplateResponse(request, self.template_name, context)

    def post_stage_1_get_user_info(self, request):
        '''
        Check user exist, if user exist check user type and return appropriate additional
        form.
        :param request: HTTP POST request fill in :class:`account.forms.UserSearchForm()`
        :type request: :class:`django.http.HttpRequest`
        :returns: :class:`django.template.response.TemplateResponse`, contain request, path to template file, and context (context contain :class:`account.forms.StudentIssueCardValidationForm()`  if user is student or :class:`account.forms.NotStudentIssueCardValidationForm()` if user is not student or :class:`account.forms.UserSearchForm()` if user not exist)
        '''
        user_search_form = UserSearchForm(data=request.POST)

        if user_search_form.is_valid():
            user = User.objects.get(username=user_search_form.cleaned_data['username'])
            user_info = {
                'fullname': user.first_name + ' ' + user.last_name,
                'borrowing_card_status': self.check_borrowing_card(user),
                'type': self.get_user_type(user),
                'username': user.username,
                'is_student': user.is_student,
            }
            context = {
                'user_search_form': UserSearchForm(),
                'user_info': user_info
            }
            if self.has_borrowing_card(user):
                return TemplateResponse(request, self.template_name, context)

            if user.is_student:
                context['issue_card_validation_form'] = StudentIssueCardValidationForm()
            else:
                context['issue_card_validation_form'] = NotStudentIssueCardValidationForm()
            self.request.session['issue_borrowing_card_user_info'] = user_info
            return TemplateResponse(request, self.template_name, context)

        context = {'user_search_form': user_search_form}
        return TemplateResponse(request, self.template_name, context)

    def post_stage_2_validate(self, request):
        '''
        Check depend on user is student or not, check validation form is valid or not. If valid then create new card and save to database

        :param request: HTTP POST request fill in :class:`account.forms.StudentIssueCardValidationForm()` or :class:`account.forms.NotStudentIssueCardValidationForm()`
        :type request: :class:`django.http.HttpRequest`

        :returns: :class:`django.template.response.TemplateResponse`, contain request, path to template file, and context (if form is valid context contain card information, that can be print to librarian screen)
        '''
        user_info = self.request.session['issue_borrowing_card_user_info']

        request.POST = request.POST.copy()
        request.POST['username'] = user_info['username']

        if user_info['is_student']:
            issue_card_validation_form = StudentIssueCardValidationForm(request.POST)
        else:
            issue_card_validation_form = NotStudentIssueCardValidationForm(request.POST)

        context = {
            'user_search_form': UserSearchForm(),
            'user_info': user_info,
            'issue_card_validation_form': issue_card_validation_form,
        }

        if issue_card_validation_form.is_valid():
            user = User.objects.get(username=user_info['username'])
            card = self.create_card(user)
            del self.request.session['issue_borrowing_card_user_info']
            context['card'] = card

        return TemplateResponse(request, self.template_name, context)

    def post(self, request):
        '''
        Check :class:`django.http.HttpRequest.user.session` and call to appropriate stage method
        :param request: HTTP POST request
        :type request: :class:`django.http.HttpRequest`
        :returns: depend on :func:`self.post_stage_1_get_user_info(self, request)` or :func:`self.post_stage_2_validate(self, request)`
        '''
        if 'issue_borrowing_card_user_info' in self.request.session:
            return self.post_stage_2_validate(request)
        return self.post_stage_1_get_user_info(request)
