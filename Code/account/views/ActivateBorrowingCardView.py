from ..forms import *
from ..models import *
from django.views import View
from django.utils.decorators import method_decorator
from django.template.response import TemplateResponse
from django.contrib.auth.decorators import login_required
from .BorrowingCardSmartProxy import BorrowingCardSmartProxy

@method_decorator(login_required(login_url='/login/'), name='dispatch')
class ActivateBorrowingCardView(BorrowingCardSmartProxy, View):
    '''
    After go to librarian to ask librarian issue new borrowing card, guest receive an activation code. 
    The guest now can activate his/her account by this code, which links to the new borrowing card.
    '''
    template_name = 'account/activate-borrowing-card.html'

    def get(self, request):
        '''
        :param request: HTTP GET request to this view
        :type request: :class:`django.http.HttpRequest`
        :returns: :class:`django.template.response.TemplateResponse`, 
                  contain request, path to template file, and context
                  (context include :class:`account.forms.ActivateBorrowingCardForm()` 
                  if user have an inactivate borrowing card, else return an error message)

        '''
        if not self.has_borrowing_card(request.user):
            context = {
                'error': 'You did not have a borrowing card, ask the Librarian to issue it first', 
            }
            return TemplateResponse(request, self.template_name, context)

        if self.has_activated_borrowing_card(request.user):
            context = {
                'success': 'You already have an activated borrowing card (card id: %d)' % self.get_card_id(request.user), 
            }
            return TemplateResponse(request, self.template_name, context)

        context = {'form': ActivateBorrowingCardForm()}
        return TemplateResponse(request, self.template_name, context)

    def post(self, request):
        '''
        Check whether user enter valid activation code
        :param request: HTTP POST request fill in :class:`account.forms.UserSearchForm()`
        :type request: :class:`django.http.HttpRequest`
        :returns: :class:`django.template.response.TemplateResponse`, contain request, 
        path to template file, and context (context contain success message if activation 
        code is correct, error message otherwise)
        '''
        request.POST = request.POST.copy()
        request.POST['username'] = request.user.username
        form = ActivateBorrowingCardForm(data=request.POST)

        if form.is_valid():
            card = Card.objects.get(user=request.user)
            card.is_activated = True
            card.save()
            context = {'success': 'Borrowing card successfully activated'}
            return TemplateResponse(request, self.template_name, context)

        context = {'form': form}
        return TemplateResponse(request, self.template_name, context)