from ..forms import *
from django.views import View
from django.shortcuts import redirect
from django.template.response import TemplateResponse

class NotStudentRegisterView(View):
  
    template_name = 'account/register.html'
  
    def post(self, request):
        if 'username' not in request.session:
            return redirect('/')

        form = NotStudentRegisterForm(data=request.POST)

        if form.is_valid():
            form.username = request.session['username']
            del request.session['username']
            form.save()

            context = {
                'form': form, 
                'message': 'Successfully registered', 
                'user': request.user,
                'redirect': True
            }
            return TemplateResponse(request, self.template_name, context)

        context = {'form': form, 'user': request.user}
        return TemplateResponse(request, self.template_name, context)