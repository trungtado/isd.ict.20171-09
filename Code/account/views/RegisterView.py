from ..forms import *
from django.views import View
from django.template.response import TemplateResponse

class RegisterView(View):
    '''Register view for guest. This view has 2 options, 
    register for student and register for not student.
    '''
    template_name = 'account/register.html'

    def get(self, request):
        '''Return RegisterForm to guest
        '''
        context = {'form': RegisterForm()}
        return TemplateResponse(request, self.template_name, context)

    def post(self, request):
        form = RegisterForm(data=request.POST)

        if form.is_valid():
            form.save()
            request.session['username'] = form.cleaned_data['username']

            if 'is_student' in form.cleaned_data:
                if form.cleaned_data['is_student']:
                    context = {'form': StudentRegisterForm(), 'action':'student/'}
                    return TemplateResponse(request, self.template_name, context)

            context = {'form': NotStudentRegisterForm(), 'action':'not-student/'}
            return TemplateResponse(request, self.template_name, context)

        context = {'form': form}
        return TemplateResponse(request, self.template_name, context)

