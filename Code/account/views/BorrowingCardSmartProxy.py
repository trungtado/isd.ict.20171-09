from ..models import *
import datetime

class BorrowingCardSmartProxy(object):

    def check_borrowing_card(self, user):
        '''
        @ Input: An library.models.User object
        @ Output: String contain borrowing card status of that object
        '''
        try:
            card = Card.objects.get(user=user)
        except Card.DoesNotExist:
            return 'user did not have borrowing card'
        else:
            if card.is_activated:
                return 'user already has an activated borrowing card'
            return 'user already has an inactivated borrowing card'

    def has_borrowing_card(self, user):
        '''
        @ Input: An library.models.User object
        @ Output: Boolean
        '''
        print user.id, user.username, Card.objects.filter(user=user).exists()
        return Card.objects.filter(user=user).exists()

    def has_activated_borrowing_card(self, user):
        '''
        @ Input: An library.models.User object
        @ Output: Boolean
        '''
        if not self.has_borrowing_card(user):
            return False
        card = Card.objects.get(user=user)
        if card.is_activated:
            return True
        return False

    def get_user_type(self, user):
        '''
        @ Input: An library.models.User object
        @ Output: String contain user type
        '''
        if user.is_student:
            return 'Student'
        return 'Not student'

    def create_card(self, user):
        '''
        TODO: this should be move to models.py
        you do not belong here
        '''
        if user.is_student:
            student = Student.objects.get(user=user)
            card = Card.objects.create(user=user, expired_date='%s-12-31' % student.study_period)
        else:
            card = Card.objects.create(user=user, expired_date=datetime.datetime.now() + datetime.timedelta(days=2*365))            
        return card

    def get_card_id(self, user):
        return Card.objects.get(user=user).id