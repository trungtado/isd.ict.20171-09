from django.views import View
from django.template.response import TemplateResponse
from book.models import *

class IndexView(View):
    template_name = 'home.html'

    def get(self, request):
        books = Book.objects.all()
        books_in_row = []
        row = []    
        i = 0
        count = 0
        for book in books:
            row.append(book)
            i += 1
            count += 1
            if i == 3:
                books_in_row.append(row)
                row = []
                row = []
                i = 0

        books_in_row.append(row)


        context = {'home_books': books_in_row}

        return TemplateResponse(request, self.template_name, context)
