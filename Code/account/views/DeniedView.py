from django.views import View
from django.template.response import TemplateResponse

class DeniedView(View):

    template_name='home.html'

    def get(self, request):
        context = {'error': 'Access denied'}
        return TemplateResponse(request, self.template_name, context)
