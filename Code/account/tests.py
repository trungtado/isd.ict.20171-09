# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.test import TestCase
from django.test import Client
from django.core.urlresolvers import reverse
from account.models import *
from django.contrib.auth.models import Group
from pprint import pprint
from django.contrib.auth import authenticate
from account.forms import *
from datetime import date

ROUTE = 'account:issue-borrowing-card'

class IssueBorrowingCardViewTestCase(TestCase):
    '''
    Test cases developed by Le Thanh - 201444075 - 01

    Because internal structure of each method in IssueBorrowingCardView
    is quite simple so I cannot develop whitebox testing

    Black box testing are the following
    '''
    def setUp(self):
        self.client = Client()
        self.student_borrower = User.objects.create(
            id=1337,
            is_superuser=0,
            first_name='Queen',
            last_name='Oliver',
            email='oliver_queen@gmail.com',
            is_staff=False,
            is_active=True,
            username='green_arrow',
            password='green_arrow',
            is_student=True,
            gender='M',
            contact='Star City',
        )
        self.student_borrower.set_password('green_arrow')
        self.student_borrower.save()
        self.student = Student.objects.create(
            user=self.student_borrower,
            student_id='20141337',
            study_period=2019
        )
        self.student.save()

        self.not_student_borrower = User.objects.create(
            id=1339,
            is_superuser=0,
            first_name='Barney',
            last_name='Stinson',
            email='barney@gmail.com',
            is_staff=False,
            is_active=True,
            username='barney',
            password='barney',
            is_student=False,
            gender='M',
            contact='NY City',
        )
        self.not_student_borrower.set_password('barney')
        self.not_student_borrower.save()
        self.not_student = NotStudent.objects.create(
            user=self.not_student_borrower,
            citizen_id='013277215'
        )
        self.not_student.save()

        self.librarian = User.objects.create(
            id=1338,
            is_superuser=0,
            first_name='Barry',
            last_name='Allen',
            email='barry_allen@gmail.com',
            is_staff=False,
            is_active=True,
            username='the_flash',
            password='the_flash',
            is_student=True,
            gender='M',
            contact='Central City',
        )
        self.librarian.set_password('the_flash')
        self.librarian.save()
        librarian_group, _ = Group.objects.get_or_create(name='Librarian')
        librarian_group.user_set.add(self.librarian)

    def test_get_permission_guest(self):
        '''guest do not have permission for this view'''
        response = self.client.get(reverse(ROUTE))
        self.assertEqual(response.status_code, 302)

    def test_get_permission_borrower(self):
        '''borrower do not have permission for this view'''
        self.client.force_login(self.student_borrower)
        response = self.client.get(reverse(ROUTE))
        self.assertEqual(response.status_code, 302)

    def test_get_permission_librarian(self):
        '''librarian must has permission for this view'''
        self.client.force_login(self.librarian)
        response = self.client.get(reverse(ROUTE))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('user_search_form' in response.context)
        self.assertIs(type(response.context['user_search_form']), UserSearchForm)

    def test_get_reset_session(self):
        '''issue borrowing card information in session must be delete'''
        self.client.force_login(self.librarian)
        '''test when session has info'''
        self.client.session['issue_borrowing_card_user_info'] = {
                'fullname': self.student_borrower.first_name + ' ' + self.student_borrower.last_name,
                'borrowing_card_status': 'active',
                'type': 'student_borrower',
                'username': self.student_borrower.username,
                'is_student': self.student_borrower.is_student,
            }
        response = self.client.get(reverse(ROUTE))
        self.assertFalse('issue_borrowing_card_user_info' in self.client.session.keys())

        '''pass test above then'''
        '''test when session do not have info'''
        response = self.client.get(reverse(ROUTE))
        self.assertFalse('issue_borrowing_card_user_info' in self.client.session.keys())

    def test_post_stage_1_username_invalid(self):
        '''enter a non exist username must receive error'''
        self.client.force_login(self.librarian)
        response = self.client.post(reverse(ROUTE), data={'username':'jibber jabber'})
        self.assertTrue('username' in response.context['user_search_form'].errors)
        self.assertTrue('User did not exist' in response.context['user_search_form'].errors['username'])

    def test_post_stage_1_student_do_not_have_card(self):
        '''must return user detail and issue card validation form'''
        self.client.force_login(self.librarian)
        response = self.client.post(
            reverse(ROUTE), 
            data={'username': self.student_borrower.username},
        )
        self.assertTrue('user_info' in response.context.keys())
        self.assertEqual(
            self.student_borrower.username,
            response.context['user_info']['username'],
        )
        self.assertTrue('issue_card_validation_form' in response.context.keys())
        self.assertIs(
            type(response.context['issue_card_validation_form']),
            StudentIssueCardValidationForm,
        )

    def test_post_stage_1_not_student_do_not_have_card(self):
        '''must return user detail and issue card validation form'''
        self.client.force_login(self.librarian)
        response = self.client.post(
            reverse(ROUTE), 
            data={'username': self.not_student_borrower.username},
        )
        self.assertTrue('user_info' in response.context.keys())
        self.assertEqual(
            self.not_student_borrower.username,
            response.context['user_info']['username'],
        )
        self.assertTrue('issue_card_validation_form' in response.context.keys())
        self.assertIs(
            type(response.context['issue_card_validation_form']),
            NotStudentIssueCardValidationForm,
        )

    def test_post_stage_1_user_already_have_card(self):
        '''must return user detail and no issue card validation form'''
        card = Card.objects.create(
            user=self.student_borrower,
            expired_date=date.today(),
        )
        card.save()

        self.client.force_login(self.librarian)
        response = self.client.post(
            reverse(ROUTE), 
            data={'username': self.student_borrower.username},
        )

        self.assertTrue('user_info' in response.context.keys())
        self.assertEqual(
            'user already has an inactivated borrowing card',
            response.context['user_info']['borrowing_card_status']
        )
        self.assertFalse('issue_card_validation_form' in response.context.keys())

    def test_post_stage_2_student_with_incorrect_student_id(self):
        '''fail, must return error'''
        self.client.force_login(self.librarian)
        response = self.client.post(
            reverse(ROUTE), 
            data={'username': self.student_borrower.username},
        )
        response = self.client.post(
            reverse(ROUTE), 
            data={'student_id': '12312312'}
        )
        self.assertTrue('student_id' in response.context['issue_card_validation_form'].errors)
        self.assertTrue(response.context['issue_card_validation_form'].errors['student_id'])

    def test_post_stage_2_student_with_correct_student_id(self):
        '''success must return card instance'''
        self.client.force_login(self.librarian)
        response = self.client.post(
            reverse(ROUTE), 
            data={'username': self.student_borrower.username},
        )
        response = self.client.post(
            reverse(ROUTE), 
            data={'student_id': self.student.student_id}
        )
        self.assertTrue('card' in response.context)

