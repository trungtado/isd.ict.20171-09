from ..models import *
from django import forms

class StudentRegisterForm(forms.ModelForm):

    username = forms.CharField(widget=forms.HiddenInput(), required=False)
    class Meta:
        model = Student
        fields = ('student_id', 'study_period')

    def save(self, commit=True):
        user = User.objects.get(username=self.username)
        student = Student(user=user, 
            student_id = self.data['student_id'], 
            study_period=self.data['study_period']
        )
        if commit:
            student.save()
        return student