from ..models import *
from django import forms
from django.core.exceptions import ValidationError

class StudentIssueCardValidationForm(forms.Form):

    username = forms.CharField(widget=forms.HiddenInput(), required=False)
    student_id = forms.CharField()

    def clean_student_id(self):
        student_id = self.cleaned_data['student_id']
        username = self.cleaned_data['username']
        if not username:
            return student_id
        user = User.objects.get(username=username)
        student = Student.objects.get(user=user)
        if student.student_id != student_id:
            raise ValidationError('student_id do not match')
        return student_id