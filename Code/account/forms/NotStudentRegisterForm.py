from ..models import *
from django import forms

class NotStudentRegisterForm(forms.ModelForm):

    username = forms.CharField(widget=forms.HiddenInput(), required=False)
    class Meta:
        model = NotStudent
        fields = ('citizen_id',)

    def save(self, commit=True):
        user = User.objects.get(username=self.username)
        not_student = NotStudent(user=user, 
            citizen_id = self.data['citizen_id'], 
        )
        if commit:
            not_student.save()
        return not_student
