from ..models import *
from django import forms
from django.core.exceptions import ValidationError

class ActivateBorrowingCardForm(forms.Form):
    username = forms.CharField(widget=forms.HiddenInput(), required=False)
    activation_code = forms.CharField()

    def clean_activation_code(self):
        username = self.cleaned_data['username']
        activation_code = self.cleaned_data['activation_code']

        user = User.objects.get(username=username)
        card = Card.objects.get(user=user)
        if card.activation_code != activation_code:
            raise ValidationError('Activation code did not match')
        
        return activation_code