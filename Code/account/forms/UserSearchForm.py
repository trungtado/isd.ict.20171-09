from ..models import *
from django import forms
from django.core.exceptions import ValidationError

class UserSearchForm(forms.Form):

    username = forms.CharField()

    def clean_username(self):
        username = self.cleaned_data['username']
        if not User.objects.filter(username=username).exists():
            raise ValidationError('User did not exist')
        return username