from ..models import *
from django import forms
from django.core.exceptions import ValidationError

class NotStudentIssueCardValidationForm(forms.Form):

    paid = forms.BooleanField()

    def clean_paid(self):
        paid = self.cleaned_data['paid']
        if not paid:
            raise ValidationError('User did not paid deposit money')
        return paid