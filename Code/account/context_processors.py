
def is_librarian(request):
    return request.user.groups.filter(name='Librarian').exists()

def user(request):
    return {
        'user': request.user,
        'librarian': is_librarian(request),
    }

