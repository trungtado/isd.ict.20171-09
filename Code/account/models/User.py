from django.db import models
from django.contrib.auth.models import User as TemplateUser

class User(TemplateUser):

    GENDERS = (('M', 'male'), ('F', 'female'))
    gender = models.CharField(max_length=2, choices=GENDERS)
    contact = models.CharField(max_length=200)
    is_student = models.BooleanField()