def randbytes(k=10):
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(k))

from .User import User
from .NotStudent import NotStudent
from .Student import Student
from .Card import Card
from .Setting import Setting