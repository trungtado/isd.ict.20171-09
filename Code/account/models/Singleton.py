from django.db import models
from django.core.cache import cache

class Singleton(models.Model):

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.pk = 1
        super(Singleton, self).save(*args, **kwargs)
        self.set_cache()

    def delete(self, *args, **kwargs):
        pass

    def set_cache(self):
        cache.set(self.__class__.__name__, self)

    @classmethod
    def load(self):
        if cache.get(self.__name__) is None:
            obj, created = self.objects.get_or_create(pk=1)
            if not created:
                obj.set_cache()
        return cache.get(self.__name__)
