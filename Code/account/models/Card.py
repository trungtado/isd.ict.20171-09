import random
import string
from .User import User
from django.db import models

def randbytes(k=10):
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(k))

class Card(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    expired_date = models.DateField()
    activation_code = models.CharField(
        max_length=10, 
        default=randbytes,
        unique=True
    )
    is_activated = models.BooleanField(default=False)

    def randbytes(self, k=10):
        return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(k))
