from django.db import models
from .Singleton import Singleton
from django.core.validators import MaxValueValidator, MinValueValidator

class Setting(Singleton):
    maximum_book_borrow = models.IntegerField(validators=[MinValueValidator(0)], default=5)
    borrow_rate = models.FloatField(validators=[MinValueValidator(0.0), MaxValueValidator(1.0)], default=0.5)
    borrow_expire = models.IntegerField(validators=[MinValueValidator(0)], default=2)
    lent_due = models.IntegerField(validators=[MinValueValidator(0)], default=14)
