from .User import User
from django.db import models

class NotStudent(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    citizen_id = models.CharField(max_length=10)