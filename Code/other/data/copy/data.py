import sqlite3
import requests
import random

conn = sqlite3.connect('../../../db.sqlite3')
cur = conn.cursor()

cur.execute('DELETE FROM book_copy')
cur.execute('DELETE FROM transaction_history')
cur.execute('DELETE FROM transaction_transaction')
conn.commit()

cur.execute('SELECT id FROM book_book')

for row in cur.fetchall():
    sequential_number = 1
    for type_ in ['A', 'R']:
        d = {
            'book_id': row[0],
            'type': type_,
            'price': random.randint(10, 100) * 1000,
            'sequential_number': sequential_number,
        }
        print d
        cur.execute('''INSERT INTO book_copy 
            (book_id, type, price, sequential_number)
            VALUES
            (?, ?, ?, ?)
            ''', (d['book_id'], d['type'], d['price'], d['sequential_number']))
        sequential_number += 1

conn.commit()