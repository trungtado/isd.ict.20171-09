.. isd-library documentation master file, created by
   sphinx-quickstart on Wed Dec  6 09:08:33 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to isd-library's documentation!
=======================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:


Account package
===============
.. automodule:: account



Views of account
----------------
.. automodule:: account.views

.. automodule:: account.views.IssueBorrowingCardView
    :members:

.. automodule:: account.views.ActivateBorrowingCardView
    :members:


Book package
===============
.. automodule:: book


Views of book
----------------
.. automodule:: book.views

.. automodule:: book.views.SearchView
    :members:

.. automodule:: book.views.RegisterView
    :members:


Transaction package
===================
.. automodule:: transaction


Views of transaction
--------------------
.. automodule:: transaction.views

.. automodule:: transaction.views.AddToCartView
    :members:

.. automodule:: transaction.views.CheckOutView
    :members:

.. automodule:: transaction.views.LentView
    :members:

.. automodule:: transaction.views.ReturnView
    :members:

