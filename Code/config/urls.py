from django.conf.urls import include, url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin

urlpatterns = [
    url(r'^', include('account.urls', namespace='account')),
    url(r'^book/', include('book.urls', namespace='book')),
    url(r'^transaction/', include('transaction.urls', namespace='transaction')),
    url(r'^admin/', admin.site.urls),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
