# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.test import TestCase
from django.test import Client
from django.core.urlresolvers import reverse
from .models import *


ROUTE = 'book:search'

class SearchViewTestCase(TestCase):
    '''
    Test cases developed by Do Trung Ta -  - 03

    Because internal structure of each method in SearchView
    is quite simple so I cannot develop whitebox testing

    Black box testing are the following
    '''
    def setUp(self):
        self.client = Client()
        classification = Classification.objects.create(label='A', description='Scientific')
        subclassification = Subclassification.objects.create(classification=classification, label='A', description='Mathematical')
        self.books = [
            Book.objects.create(
                sequential_number=1,
                subclassification=subclassification,
                title='linear algebra',
                publisher='Springer',
                authors='Dan Boneh',
                isbn='9999999999',
                description='matrix and stuff like that',
                cover=''
            ),
            Book.objects.create(
                sequential_number=2,
                subclassification=subclassification,
                title='harry potter',
                publisher='Van hoc',
                authors='JK Rowling',
                isbn='8888888888',
                description='wizard and stuff like that',
                cover=''
            ),
        ]

    def test_permission_guest(self):
        '''guest has permission for this view'''
        response = self.client.get(reverse(ROUTE))
        self.assertEqual(response.status_code, 200)

    def test_null(self):
        '''return all result'''
        response = self.client.get(reverse(ROUTE))
        self.assertEqual(len(response.context['books'][0]), 2)

    def test_query(self):
        '''return one match result'''
        response = self.client.get(reverse(ROUTE) + '?query=harry')
        self.assertEqual(len(response.context['books'][0]), 1)

    def test_title(self):
        '''return one match result'''
        response = self.client.get(reverse(ROUTE) + '?title=harry')
        self.assertEqual(len(response.context['books'][0]), 1)

    def test_wrong_query(self):
        '''return zero match result'''
        response = self.client.get(reverse(ROUTE) + '?query=aaaaaaa')
        self.assertEqual(len(response.context['books'][0]), 0)

    def test_wrong_title(self):
        '''return zero match result'''
        response = self.client.get(reverse(ROUTE) + '?title=aaaaaaa')
        self.assertEqual(len(response.context['books'][0]), 0)


class RegisterViewTestCase(TestCase):
    '''
    Test cases developed by Do Trung Ta -  - 03

    Because internal structure of each method in RegisterView
    is quite simple so I cannot develop whitebox testing

    Black box testing are the following
    '''
    def setUp(self):
        pass