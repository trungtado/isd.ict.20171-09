from ..forms import *
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.template.response import TemplateResponse

@method_decorator([csrf_exempt], name='dispatch')
class BackdoorRegisterView(View):

    template_name = 'book/book.html'

    def get(self, request):
        context = {'register_book_form':RegisterBookForm()}
        return TemplateResponse(request, self.template_name, context)
        
    def post(self, request):

        register_book_form = RegisterBookForm(request.POST, request.FILES)

        if register_book_form.is_valid():
            register_book_form.save()
            context = {'register_book_form': RegisterBookForm(), 'message':'Successfully registered'}
            return TemplateResponse(request, self.template_name, context)

        context = {'register_book_form': register_book_form}
        return TemplateResponse(request, self.template_name, context)
