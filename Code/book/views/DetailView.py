from ..models import *
from ..forms import *
from django.views.generic.detail import DetailView as TemplateDetailView
from transaction.models import *

class DetailView(TemplateDetailView):

    model = Book
    template_name = 'book/book.html'
    
    def get_copy_status(self, copy):
        try:
            transaction = Transaction.objects.get(copy=copy)
        except Transaction.DoesNotExist:
            return 'available'
        else:
            return transaction.type.replace('B', 'borrowed').replace('L', 'lent')

    def get_copy_information(self, book):
        copies = []
        for copy in Copy.objects.filter(book=book):
            copies.append({
                'sequential_number': copy.sequential_number,
                'type': copy.type.replace('A', 'available').replace('R', 'reference only'),
                'price': copy.price,
                'status': self.get_copy_status(copy),
            })
        return copies

    def get_context_data(self, **kwargs):
        context = super(DetailView, self).get_context_data(**kwargs)
        book = kwargs['object']
        context['book'] = book
        context['subclassification'] = book.subclassification
        context['classification'] = book.subclassification.classification
        context['copies'] = self.get_copy_information(book)
        return context
