'''
Contain bussiness logic code for usecases
'''

from .SearchView import SearchView
from .RegisterView import RegisterView
from .BackdoorRegisterView import BackdoorRegisterView
from .DetailView import DetailView