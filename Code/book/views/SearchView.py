from ..forms import *
from ..models import *
from django.views import View
from django.db.models import Q
from django.core.paginator import Paginator
from django.template.response import TemplateResponse
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator

@method_decorator([csrf_exempt], name='dispatch')
class SearchView(View):
    '''
    Guests can search book information by title, classification, author or publisher.
    Or they can simply search all fields of book by a query string.
    '''
    template_name = 'book/book.html'

    def search(self, cleaned_data):
        '''
        :param cleaned_data: contain these following keys
                             
                             * title: string to match with book title

                             * classification_id: id to filter book with that classification id

                             * authors: string to match with book author

                             * publisher: string to match with book publisher

                             * query: string to match with all fields

        :type cleaned_data: dictionary 

        :returns: :class:`django.db.models.query.QuerySet`, contain queryset of :class:`book.models.Book`
        '''
        query = cleaned_data['query']            
        if query:
            return Book.objects.filter(
                Q(title__contains=query) | 
                Q(subclassification__label__contains=query) |
                Q(subclassification__classification__label__contains=query) |
                Q(authors__contains=query) | 
                Q(publisher__contains=query)
            )
        books = Book.objects.all()
        if cleaned_data['title']: 
            books = books.filter(
                title__contains=cleaned_data['title'])
        classification_id = cleaned_data.get('classification_id')
        if classification_id:
            if int(classification_id): 
                books = books.filter(
                    subclassification__classification_id=cleaned_data['classification_id'])
        if cleaned_data['authors']: 
            books = books.filter(
                authors__contains=cleaned_data['authors'])
        if cleaned_data['publisher']: 
            books = books.filter(
                publisher__contains=cleaned_data['publisher'])
        return books

    def get(self, request):
        '''
        :param request: HTTP GET request to this view (contain search query in GET parameters)
        :type request: :class:`django.http.HttpRequest`

        :returns: :class:`django.template.response.TemplateResponse`, 
                  contain request, path to template file, and context
                  
                  context is dictionary include

                  * search_book_form : :class:`book.forms.SearchBookForm()` with submited GET parameters

                  * books : list of tuples of 3 :class:`book.models.Book()` for template to render three book in one row

                  * count : int total number of book
        '''
        search_book_form = SearchBookForm(request.GET)
        page = request.GET.get('page', 1)
        request.GET = request.GET.copy()

        if search_book_form.is_valid():
            books = self.search(search_book_form.cleaned_data)
        else:
            books = []

        # Bad code in written in really low energy status
        # Don't blame me

        books_in_row = []
        row = []    
        i = 0
        count = 0
        for book in books:
            row.append(book)
            i += 1
            count += 1
            if i == 3:
                books_in_row.append(row)
                row = []
                row = []
                i = 0

        books_in_row.append(row)

        context = {
            'search_book_form': search_book_form,
            'books': books_in_row,
            'count': count,
        }

        return TemplateResponse(request, self.template_name, context)


