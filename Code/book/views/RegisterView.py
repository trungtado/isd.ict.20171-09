from ..forms import *
from ..models import *
from django.views import View
from django.template.response import TemplateResponse
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, user_passes_test

@method_decorator(
    [login_required(login_url='/login/'), 
    user_passes_test(lambda u: u.groups.filter(name='Librarian').count() == 1, login_url='/denied/')], 
    name='dispatch')
class RegisterView(View):
    '''
    The librarian may register new books into the library. This registration can be addition of a new
    copy or creation of a new book entry and copy.
    '''
    template_name = 'book/book.html'

    def get(self, request):
        '''
        Delete cache in :class:`django.http.HttpRequest.user.session` then 
        return form for librarian to enter ISBN number

        :param request: HTTP GET request to this view
        :type request: :class:`django.http.HttpRequest`

        :returns: :class:`django.template.response.TemplateResponse`, 
                  contain request, path to template file, and context

                  context include :class:`book.forms.IsbnForm()`
        '''
        if 'register_book_info' in request.session:
            del request.session['register_book_info']
        context = {'isbn_form': IsbnForm()}
        return TemplateResponse(request, self.template_name, context)

    def post_stage_1_check_isbn(self, request):
        '''
        Check whether ISBN number librarian entered is existed or not. 
        Save those information to :class:`django.http.HttpRequest.user.session`
    
        Example

        .. code-block:: python
            :emphasize-lines: 1-4

            request.session['register_book_info'] = {
                'isbn': '912930123831231',
                'existed': True,
            }


        :param request: HTTP POST request fill in :class:`book.forms.IsbnForm()`
        :type request: :class:`django.http.HttpRequest`
        :returns: :class:`django.template.response.TemplateResponse`, contain request, path to template file, and context. If submitted ISBN is existed, return that :class:`book.models.Book` instance and :class:`book.forms.RegisterCopyForm()` otherwise return :class:`book.forms.RegisterBookForm()` 
        '''
        isbn_form = IsbnForm(request.POST)

        if isbn_form.is_valid():
            register_book_info = {
                'isbn': isbn_form.cleaned_data['isbn'],
                'existed': self.isbn_existed(isbn_form.cleaned_data['isbn'])
            }
            context = {
                'register_book_info': register_book_info, 
            }
            if register_book_info['existed']:
                context['register_copy_form'] = RegisterCopyForm()
                context['book'] = Book.objects.get(isbn=register_book_info['isbn'])
            else:
                context['register_book_form'] = RegisterBookForm()
            request.session['register_book_info'] = register_book_info
            return TemplateResponse(request, self.template_name, context)

        context = {'isbn_form': isbn_form}
        return TemplateResponse(request, self.template_name, context)

    def post_stage_2_flow_1_register_book(self, request):
        '''
        Verify librarian input on :class:`book.forms.RegisterBookForm()` and save new book instance to database

        :param request: HTTP POST request fill in :class:`book.forms.RegisterBookForm()`
        :type request: :class:`django.http.HttpRequest`
        :returns: :class:`django.template.response.TemplateResponse`, contain request, path to template file, and context.`. If form is correct then return success message and new :class:`book.models.Book()` instance, otherwise, return the form with error message.        
        '''
        register_book_form = RegisterBookForm(request.POST)

        if register_book_form.is_valid():
            register_book_form.save()
            request.session['register_book_info']['existed'] = True
            context = {
                'success': 'Successfully registered book, now register copies of that book',
                'register_copy_form': RegisterCopyForm(),
                'book': Book.objects.get(isbn=request.session['register_book_info']['isbn']),
            }
            return TemplateResponse(request, self.template_name, context)

        context = {'register_book_form': register_book_form}
        return TemplateResponse(request, self.template_name, context)

    def post_stage_2_flow_2_register_copy(self, request):
        '''
        Verify librarian input on :class:`book.forms.RegisterCopyForm()` and save new book instance to database

        :param request: HTTP POST request fill in :class:`book.forms.RegisterCopyForm()`
        :type request: :class:`django.http.HttpRequest`
        :returns: :class:`django.template.response.TemplateResponse`, contain request, path to template file, and context.`. If form is correct then return success message and new :class:`book.models.Copy()` instance, otherwise, return the form with error message.
        '''
        request.POST = request.POST.copy()
        request.POST['isbn'] = request.session['register_book_info']['isbn']
        register_copy_form = RegisterCopyForm(request.POST)

        if register_copy_form.is_valid():
            register_copy_form.save()
            context = {
                'success': 'Successfully registered copies, now register more copies.',
                'register_copy_form': RegisterCopyForm(),
                'book': Book.objects.get(isbn=request.session['register_book_info']['isbn']),
            }
            return TemplateResponse(request, self.template_name, context)

        context = {'register_copy_form': register_copy_form}
        return TemplateResponse(request, self.template_name, context)

    def post(self, request):
        '''
        Check request.session and redirect to appropriate post stage method
        :param request: HTTP POST request fill in :class:`book.forms.IsbnForm()`
        :type request: :class:`django.http.HttpRequest`
        :returns: same as the post stage method returns
        '''
        if 'register_book_info' in request.session:
            if request.session['register_book_info']['existed']:
                return self.post_stage_2_flow_2_register_copy(request)
            return self.post_stage_2_flow_1_register_book(request)
        return self.post_stage_1_check_isbn(request)

    def isbn_existed(self, isbn):
        '''
        Check if is there any book with entered isbn
        :param isbn: unique ISBN 13 of a book
        :type isbn: string
        :returns: boolean, True if book with that isbn is existed
        '''
        if Book.objects.filter(isbn=isbn).exists():
            return True
        return False