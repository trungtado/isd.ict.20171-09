from ..models import *
from django import forms
from django.db import models
from django.core.exceptions import ValidationError

class RegisterCopyForm(forms.ModelForm):

    isbn = forms.CharField(widget=forms.HiddenInput(), required=False)
    number_of_copy = forms.IntegerField()

    class Meta:
        model = Copy
        fields = ('type', 'price')

    def clean_number_of_copy(self):
        n = self.cleaned_data['number_of_copy']
        if n > 10:
            raise ValidationError('Number of copy must be less than 10')
        if n <= 0:
            raise ValidationError('Number of copy must be greater than zero')
        return n

    def clean_price(self):
        price = self.cleaned_data['price']
        if price <= 0:
            raise ValidationError('Price must be positive integer')
        return price

    def save(self):
        book = Book.objects.get(isbn=self.cleaned_data['isbn'])
        self.cleaned_data['book'] = book
        self.cleaned_data['sequential_number'] = Copy.objects.filter(book=book).\
            aggregate(models.Max('sequential_number'))['sequential_number__max']
        if not self.cleaned_data['sequential_number']:
            self.cleaned_data['sequential_number'] = 0
        del self.cleaned_data['isbn']
        n = self.cleaned_data['number_of_copy']
        del self.cleaned_data['number_of_copy']
        for _ in range(n):
            self.cleaned_data['sequential_number'] += 1
            copy = Copy.objects.create(**self.cleaned_data)
            copy.save()

        return copy
        