from django import forms
from string import digits
from django.core.exceptions import ValidationError

class IsbnForm(forms.Form):
    isbn = forms.CharField()

    def clean_isbn(self):
        isbn = self.cleaned_data['isbn']
        for c in isbn:
            if c not in digits:
                raise forms.ValidationError('ISBN must contain only digits')
        return isbn