from ..models import *
from django import forms
from django.core.exceptions import ValidationError

class SearchBookForm(forms.Form):

    try:
        CHOICES = [(item.id, item.label + ' ' + item.description) for item in Classification.objects.all()]
    except:
        CHOICES = []
    CHOICES = tuple([(0, None)] + CHOICES)

    title = forms.CharField(required=False)
    classification_id = forms.ChoiceField(choices=CHOICES, required=False)
    authors = forms.CharField(required=False)
    publisher = forms.CharField(required=False)
    query = forms.CharField(required=False)