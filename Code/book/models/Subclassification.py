from django.db import models
from .Classification import Classification

class Subclassification(models.Model):
    
    label = models.CharField(max_length=5)
    description = models.TextField()
    classification = models.ForeignKey(Classification, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('label', 'classification')