from django.db import models
from .Book import Book

class Copy(models.Model):

    TYPES = (
        ('R', 'Reference'), 
        ('A', 'Borrowable'),
    )
    sequential_number = models.IntegerField()
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    type = models.CharField(max_length=1, choices=TYPES)
    price = models.FloatField()
    
    class Meta:
        unique_together = ('sequential_number', 'book')
        