from django.db import models

class Classification(models.Model):

    label = models.CharField(max_length=1)
    description = models.TextField()

    class Meta:
        unique_together = ('label', )