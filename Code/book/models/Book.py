from django.db import models
from .Subclassification import Subclassification

class Book(models.Model):

    sequential_number = models.IntegerField()
    subclassification = models.ForeignKey(Subclassification)
    title = models.CharField(max_length=300)
    publisher = models.CharField(max_length=300)
    authors = models.CharField(max_length=300)
    isbn = models.CharField(max_length=30, unique=True)
    description = models.TextField()
    cover = models.ImageField(upload_to='', blank=True)

    class Meta:
        unique_together = ('sequential_number', 'subclassification')
        ordering = ['-id']