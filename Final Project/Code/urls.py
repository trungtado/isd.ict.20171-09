from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.static import static
from config import settings
from account.views import IndexView

urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^admin/', admin.site.urls),
    url(r'^account/', include('account.urls', namespace='account')),
    url(r'^book/', include('book.urls', namespace='book')),
    url(r'^transaction/', include('transaction.urls', namespace='transaction')),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)