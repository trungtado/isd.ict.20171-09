import os
import string
import random

def randbytes(k=10):
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(k))

def rand_img_path(instance, filename):
    ls = filename.split('.')
    postfix = ls[len(ls)-1]
    basename = randbytes() + '.' + postfix
    path = os.path.join('media/book_cover', basename)
    while os.path.exists(path):
        basename = randbytes()
        path = os.path.join('media/book_cover', basename)
    return os.path.join('book_cover', basename)
