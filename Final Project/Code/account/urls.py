from django.conf.urls import url
from django.contrib.auth import views as auth_views
from .views import *

urlpatterns = [
    url(r'^login/$', LoginView.as_view(), name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': '/'}, name='logout'),
    url(r'^register/$', RegisterView.as_view(), name='register'),
    url(r'^register/student/$', StudentRegisterView.as_view()),
    url(r'^register/not_student/$', NotStudentRegisterView.as_view()),
    url(r'^issue_borrowing_card/$', IssueBorrowingCardView.as_view(), name='issue_borrowing_card'),
    url(r'^activate_borrowing_card/$', ActivateBorrowingCardView.as_view(), name='activate_borrowing_card'),
]
