from django import forms
from django.db import models
from .models import *
from django.core.exceptions import ValidationError

class RegisterForm(forms.ModelForm):

    error_messages = {
        'password_mismatch': ('The two password fields didn\'t match.'),
    }
    password1 = forms.CharField(label=('Password'),
        widget=forms.PasswordInput)
    password2 = forms.CharField(label=('Password confirmation'),
        widget=forms.PasswordInput,
        help_text=('Enter the same password as above, for verification.'))

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'gender', 'contact', 'is_student')

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2

    def save(self, commit=True):
        user = super(RegisterForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        if commit:
            user.save()
        return user


class StudentRegisterForm(forms.ModelForm):

    username = forms.CharField(widget=forms.HiddenInput(), required=False)
    class Meta:
        model = Student
        fields = ('student_id', 'study_period')

    def save(self, commit=True):
        user = User.objects.get(username=self.username)
        student = Student(user=user, 
            student_id = self.data['student_id'], 
            study_period=self.data['study_period']
        )
        if commit:
            student.save()
        return student


class NotStudentRegisterForm(forms.ModelForm):

    username = forms.CharField(widget=forms.HiddenInput(), required=False)
    class Meta:
        model = NotStudent
        fields = ('citizen_id',)

    def save(self, commit=True):
        user = User.objects.get(username=self.username)
        not_student = NotStudent(user=user, 
            citizen_id = self.data['citizen_id'], 
        )
        if commit:
            not_student.save()
        return not_student


class UserSearchForm(forms.Form):

    username = forms.CharField()

    def clean_username(self):
        username = self.cleaned_data['username']
        if not User.objects.filter(username=username).exists():
            raise ValidationError('User did not exist')
        return username


class StudentIssueCardValidationForm(forms.Form):

    username = forms.CharField(widget=forms.HiddenInput(), required=False)
    student_id = forms.CharField()

    def clean_student_id(self):
        student_id = self.cleaned_data['student_id']
        username = self.cleaned_data['username']
        if not username:
            print 'username:', username
            return student_id
        user = User.objects.get(username=username)
        student = Student.objects.get(user=user)
        print 'debug:', student.student_id, student_id, student.student_id != student_id
        if student.student_id != student_id:
            raise ValidationError('student_id do not match')
        return student_id


class NotStudentIssueCardValidationForm(forms.Form):

    paid = forms.BooleanField()

    def clean_paid(self):
        paid = self.cleaned_data['paid']
        if not paid:
            raise ValidationError('User did not paid deposit money')
        return paid


class ActivateBorrowingCardForm(forms.Form):
    username = forms.CharField(widget=forms.HiddenInput(), required=False)
    activation_code = forms.CharField()

    def clean_activation_code(self):
        username = self.cleaned_data['username']
        activation_code = self.cleaned_data['activation_code']

        user = User.objects.get(username=username)
        card = Card.objects.get(user=user)
        if card.activation_code != activation_code:
            raise ValidationError('Activation code did not match')
        
        return activation_code
