from .views import *
from .models import *
from django.core.urlresolvers import reverse
from django.test import TestCase, Client

class IssueBorrowingCardViewTestCase(TestCase):

    def setUp(self):
        self.client = Client()
        user = User.objects.get_or_create(
            username='thanh',
            password='1',
            first_name='Thanh',
            last_name='Le',
            email='thanh@gmail.com',
            gender='M',
            contact='HUST',
            is_student=True
        )
        self.client.force_login(user[0])

    def test_get(self):
        response = self.client.get('/account/issue_borrowing_card/')
        status_code = response.status_code
        self.assertEqual(status_code, 200)