# -*- coding:utf-8 -*- #
import datetime
from .models import *
from .forms import *
from pprint import pprint 
from config import settings
from django.template import loader
from django.template.response import TemplateResponse
from django.shortcuts import render, redirect, resolve_url
from django.http import HttpResponse, HttpResponseRedirect
from django.views import View
from django.views.generic.detail import DetailView
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, ListView
from django.contrib.auth.decorators import login_required
from django.contrib.auth import REDIRECT_FIELD_NAME, login as auth_login
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.utils.http import is_safe_url
from django.utils.decorators import method_decorator

APPNAME = 'account'

@method_decorator(login_required, name='dispatch')
class IndexView(TemplateView):
 
    template_name = 'index.html'

    def get(self, request):
        context = {'user': request.user}
        return TemplateResponse(request, self.template_name, context)


@method_decorator(csrf_exempt, name='dispatch')
class LoginView(View):
  
    template_name = '%s/login.html' % APPNAME

    def get(self, request):
        context = {'form': AuthenticationForm(request), 'user': request.user}
        return TemplateResponse(request, self.template_name, context)

    def post(self, request):
        form = AuthenticationForm(request, data=request.POST)
        redirect_to = request.POST.get(REDIRECT_FIELD_NAME,'')

        if form.is_valid():
            if not is_safe_url(url=redirect_to, host=request.get_host()):
                redirect_to = resolve_url(settings.LOGIN_REDIRECT_URL)

            auth_login(request, form.get_user())

            return HttpResponseRedirect(redirect_to)

        context = {'form': form, 'user': request.user}
        return TemplateResponse(request, self.template_name, context)


@method_decorator(csrf_exempt, name='dispatch')
class RegisterView(View):
 
    template_name = '%s/register.html' % APPNAME

    def get(self, request):
        context = {'form': RegisterForm(), 'user': request.user}
        return TemplateResponse(request, self.template_name, context)

    def post(self, request):
        form = RegisterForm(data=request.POST)

        if form.is_valid():
            form.save()
            request.session['username'] = form.cleaned_data['username']

            if 'is_student' in form.cleaned_data:
                if form.cleaned_data['is_student']:
                    context = {'form': StudentRegisterForm(), 'action':'student/', 'user': request.user}
                    return TemplateResponse(request, self.template_name, context)

            context = {'form': NotStudentRegisterForm(), 'action':'not_student/', 'user': request.user}
            return TemplateResponse(request, self.template_name, context)

        context = {'form': form, 'user': request.user}
        return TemplateResponse(request, self.template_name, context)

    def student_information(self, request):
        return HttpResponse('hello world')


@method_decorator(csrf_exempt, name='dispatch')
class StudentRegisterView(View):
   
    template_name = '%s/register.html' % APPNAME

    def post(self, request):
        if 'username' not in request.session:
            return redirect('/')

        form = StudentRegisterForm(data=request.POST)

        if form.is_valid():
            form.username = request.session['username']
            del request.session['username']
            form.save()

            context = {
                'form': form, 
                'message': 'Successfully registered', 
                'user': request.user,
                'redirect': True
            }
            return TemplateResponse(request, self.template_name, context)

        context = {'form': form, 'user': request.user}
        return TemplateResponse(request, self.template_name, context)


@method_decorator(csrf_exempt, name='dispatch')
class NotStudentRegisterView(View):
  
    template_name = '%s/register.html' % APPNAME
  
    def post(self, request):
        if 'username' not in request.session:
            return redirect('/')

        form = NotStudentRegisterForm(data=request.POST)

        if form.is_valid():
            form.username = request.session['username']
            del request.session['username']
            form.save()

            context = {
                'form': form, 
                'message': 'Successfully registered', 
                'user': request.user,
                'redirect': True
            }
            return TemplateResponse(request, self.template_name, context)

        context = {'form': form, 'user': request.user}
        return TemplateResponse(request, self.template_name, context)


@method_decorator([csrf_exempt, login_required], name='dispatch')
class BorrowingCardView(View):

    def check_borrowing_card(self, user):
        '''
        @ Input: An library.models.User object
        @ Output: String contain borrowing card status of that object
        '''
        try:
            card = Card.objects.get(user=user)
        except Card.DoesNotExist:
            return 'user did not have borrowing card'
        else:
            if card.is_activated:
                return 'user already has an activated borrowing card'
            return 'user already has an inactivated borrowing card'

    def has_borrowing_card(self, user):
        '''
        @ Input: An library.models.User object
        @ Output: Boolean
        '''
        print user.id, user.username, Card.objects.filter(user=user).exists()
        return Card.objects.filter(user=user).exists()

    def has_activated_borrowing_card(self, user):
        '''
        @ Input: An library.models.User object
        @ Output: Boolean
        '''
        if not self.has_borrowing_card(user):
            return False
        card = Card.objects.get(user=user)
        if card.is_activated:
            return True
        return False

    def get_user_type(self, user):
        '''
        @ Input: An library.models.User object
        @ Output: String contain user type
        '''
        if user.is_student:
            return 'Student'
        return 'Not student'

    def create_card(self, user):
        '''
        TODO: this should be move to models.py
        you do not belong here
        '''
        if user.is_student:
            student = Student.objects.get(user=user)
            card = Card.objects.create(user=user, expired_date='%s-12-31' % student.study_period)
        else:
            card = Card.objects.create(user=user, expired_date=datetime.datetime.now() + datetime.timedelta(days=2*365))            
        return card


@method_decorator([csrf_exempt, login_required], name='dispatch')
class IssueBorrowingCardView(BorrowingCardView):

    template_name = '%s/issue_borrowing_card.html' % APPNAME

    def get(self, request):
        if 'issue_borrowing_card_user_info' in request.session:
            del request.session['issue_borrowing_card_user_info']
        context = {'user_search_form': UserSearchForm(), 'user': request.user}
        return TemplateResponse(request, self.template_name, context)

    def post_stage_1_get_user_info(self, request):
        user_search_form = UserSearchForm(data=request.POST)

        if user_search_form.is_valid():
            user = User.objects.get(username=user_search_form.cleaned_data['username'])
            user_info = {
                'fullname': user.first_name + ' ' + user.last_name,
                'borrowing_card_status': self.check_borrowing_card(user),
                'type': self.get_user_type(user),
                'username': user.username,
                'is_student': user.is_student,
            }
            context = {
                'user_search_form': UserSearchForm(),
                'user_info': user_info, 'user': request.user
            }
            if self.has_borrowing_card(user):
                return TemplateResponse(request, self.template_name, context)

            if user.is_student:
                context['issue_card_validation_form'] = StudentIssueCardValidationForm()
            else:
                context['issue_card_validation_form'] = NotStudentIssueCardValidationForm()
            self.request.session['issue_borrowing_card_user_info'] = user_info
            return TemplateResponse(request, self.template_name, context)

        context = {'user_search_form': user_search_form}
        return TemplateResponse(request, self.template_name, context)

    def post_stage_2_validate(self, request):
        user_info = self.request.session['issue_borrowing_card_user_info']

        request.POST = request.POST.copy()
        request.POST['username'] = user_info['username']

        if user_info['is_student']:
            issue_card_validation_form = StudentIssueCardValidationForm(request.POST)
        else:
            issue_card_validation_form = NotStudentIssueCardValidationForm(request.POST)

        context = {
            'user_search_form': UserSearchForm(),
            'user_info': user_info,
            'issue_card_validation_form': issue_card_validation_form,
            'user': request.user,
        }

        if issue_card_validation_form.is_valid():
            user = User.objects.get(username=user_info['username'])
            card = self.create_card(user)
            del self.request.session['issue_borrowing_card_user_info']
            context['card'] = card

        return TemplateResponse(request, self.template_name, context)

    def post(self, request):
        if 'issue_borrowing_card_user_info' in self.request.session:
            return self.post_stage_2_validate(request)
        return self.post_stage_1_get_user_info(request)


@method_decorator([csrf_exempt, login_required], name='dispatch')
class ActivateBorrowingCardView(BorrowingCardView):

    template_name = '%s/activate_borrowing_card.html' % APPNAME

    def get(self, request):
        if not self.has_borrowing_card(request.user):
            context = {
                'message': 'You did not have a borrowing card, ask the Librarian to issue it first', 
                'user': request.user
            }
            return TemplateResponse(request, self.template_name, context)

        if self.has_activated_borrowing_card(request.user):
            context = {
                'message': 'You already have an activated borrowing card', 
                'user': request.user
            }
            return TemplateResponse(request, self.template_name, context)

        context = {'activate_borrowing_card_form': ActivateBorrowingCardForm()}
        return TemplateResponse(request, self.template_name, context)

    def post(self, request):
        request.POST = request.POST.copy()
        request.POST['username'] = request.user.username
        activate_borrowing_card_form = ActivateBorrowingCardForm(data=request.POST)

        if activate_borrowing_card_form.is_valid():
            card = Card.objects.get(user=request.user)
            card.is_activated = True
            card.save()
            context = {'message': 'Borrowing card successfully activated', 'user': request.user}
            return TemplateResponse(request, self.template_name, context)

        context = {'activate_borrowing_card_form': activate_borrowing_card_form, 'user': request.user}
        return TemplateResponse(request, self.template_name, context)