from django.db import models
from django.contrib.auth.models import User as TemplateUser

def randbytes(k=10):
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(k))

class User(TemplateUser):

    GENDERS = (('M', 'male'), ('F', 'female'))
    gender = models.CharField(max_length=2, choices=GENDERS)
    contact = models.CharField(max_length=200)
    is_student = models.BooleanField()


class Student(models.Model):

    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    student_id = models.CharField(max_length=8)
    study_period = models.IntegerField(default=2019)


class NotStudent(models.Model):

    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    citizen_id = models.CharField(max_length=10)


class Card(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    expired_date = models.DateField()
    activation_code = models.CharField(
        max_length=10, 
        default=randbytes,
        unique=True
    )
    is_activated = models.BooleanField(default=False)
