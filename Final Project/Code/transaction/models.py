from account.models import *
from book.models import *
from django.db import models
from django.core.exceptions import ValidationError

TYPES = (('B', 'Borrow'), ('L', 'Lent'))
MAX_COPIES_PER_CARD = 5
DUES = {'B': 2, 'L': 14}

class Transaction(models.Model):

    type = models.CharField(max_length=2, choices=TYPES)
    start = models.DateField(auto_now=True)
    card = models.ForeignKey(Card)
    copy = models.OneToOneField(Copy, unique=True)

    def get_due(self):
        return self.start + datetime.timedelta(days=DUES[self.type])

    def clean_card(self):
        if self.objects.filter(card=self.card).count() >= MAX_COPY_PER_CARD:
            raise ValidationError('Exceed maximum number of copies per card')
        return self.card


class History(models.Model):

    type = models.CharField(max_length=2, choices=TYPES)
    start = models.DateField()
    end = models.DateField(auto_now=True, null=True)
    card = models.ForeignKey(Card)
    copy = models.ForeignKey(Copy)
    description = models.TextField()


# Notification