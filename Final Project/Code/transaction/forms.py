from .models import *
from django import forms
from django.core.exceptions import ValidationError

class AddToCartForm(forms.Form):

    book_id = forms.IntegerField(widget=forms.HiddenInput(), required=False)

    def clean_book_id(self):
        book_id = self.cleaned_data['book_id']
        try:
            book = Book.objects.get(id=book_id)
        except Book.DoesNotExist:
            raise ValidationError('Book is not existed')
        else:
            for copy in Copy.objects.filter(book=book, type='A'):
                if not Transaction.objects.filter(copy=copy).exists():
                    return {'book_id': book_id, 'copy_id': copy.id}
            raise ValidationError('Book is not avalable')
        

class SearchBorrowInformationForm(forms.Form):

    username = forms.CharField()

    def clean_username(self):
        username = self.cleaned_data['username']
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            raise ValidationError('username does not exist')
        else:
            try:
                card = Card.objects.get(user=user)
            except Card.DoesNotExist:
                raise ValidationError('user does not have borrowing card')
            else:
                if not card.is_activated:
                    raise ValidationError('user has an inactive borrowing card')
                return username


class LentForm(forms.Form):

    def __init__(self, transactions, *args, **kwargs):
        super(LentForm, self).__init__(*args, **kwargs)
        CHOICES = tuple([(transaction.id, transaction.copy.book.title + ' ' + str(transaction.copy.id) ) for transaction in transactions])
        self.fields['transactions'] = forms.TypedMultipleChoiceField(choices=CHOICES, widget=forms.CheckboxSelectMultiple)


class SearchLentInformationForm(forms.Form):

    card_id = forms.IntegerField(required=False)
    sequence_number = forms.IntegerField(required=False)

    def clean_borrower_card_id(self):
        card_id = self.cleaned_data['borrower_card_id']
        if not Card.objects.filter(id=id).exists():
            raise ValidationError('Card does not exist')
        return borrower_card_id

    def clean_copy_sequence_number(self):
        sequence_number = self.cleaned_data['sequence_number']
        if not Copy.objects.filter(sequence_number=sequence_number).exists():
            raise ValidationError('Copy does not exist')
        return sequence_number


class VerifyLentInformationForm(forms.Form):

    transaction_id = forms.IntegerField(widget=forms.HiddenInput(), required=False)
    book_title = forms.CharField()
    borrower_first_name = forms.CharField()
    borrower_last_name = forms.CharField()

    def clean_book_name(self):
        transaction_id = self.cleaned_data['transaction_id']
        transaction = Transaction.objects.get(id=transaction_id)
        book_title = self.cleaned_data['book_title']
        if transaction.copy.book.title.lower() != book_title.lower():
            raise ValidationError('Book title not match')
        return book_name

    def clean_borrower_first_name(self):
        transaction_id = self.cleaned_data['transaction_id']
        transaction = Transaction.objects.get(id=transaction_id)
        borrower_first_name = self.cleaned_data['borrower_first_name']
        if transaction.card.user.first_name != borrower_first_name:
            raise ValidationError('Borrower first name do not match')
        return borrower_first_name

    def clean_borrower_last_name(self):
        transaction_id = self.cleaned_data['transaction_id']
        transaction = Transaction.objects.get(id=transaction_id)
        borrower_last_name = self.cleaned_data['borrower_last_name']
        if transaction.card.user.last_name != borrower_last_name:
            raise ValidationError('Borrower last name do not match')
        return borrower_last_name


class CompensationForm(forms.Form):
    compensation_money = forms.IntegerField(initial=0)

    def clean_compensation_money(self):
        compensation_money = self.cleaned_data['compensation_money']
        if compensation_money < 0:
            raise ValidationError('Compensation money cannot be negative')
        return compensation_money