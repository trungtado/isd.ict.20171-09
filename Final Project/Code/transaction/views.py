from .forms import *
from .models import *
from datetime import date
from pprint import pprint 
from config import settings
from django.views import View
from django.core.exceptions import ValidationError
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.template.response import TemplateResponse
from django.contrib.auth.decorators import login_required


APPNAME = 'transaction'
MAX_COPIES_PER_CARD = 5


@method_decorator([csrf_exempt, login_required], name='dispatch')
class CartView(View):
    template_name = '%s/cart.html' % APPNAME

    def get(self, request):
        self.make_sure_cart_in_session(request)
        context = self.get_context(request)
        return TemplateResponse(request, self.template_name, context)

    def get_context(self, request, error=''):
        cart = request.session['cart']
        books = Book.objects.filter(id__in=[item['book_id'] for item in cart])
        context = {
            'books': books, 
            'error': error,
        }
        return context

    def in_possession(self, book, request):
        self.make_sure_cart_in_session(request)
        card = Card.objects.get(user=request.user)
        for transaction in Transaction.objects.filter(card=card):
            if transaction.copy.book.id == book['book_id']:
                return True
        for item in request.session['cart']:
            if item == book:
                return True
        return False

    def has_card(self, user):
        return Card.objects.filter(user=user).exists()

    def card_expired(self, user):
        card = Card.objects.get(user=user)
        if date.today() > card.expired_date:
            return True
        return False

    def exceed_max_copies(self, user, cart):
        card = Card.objects.get(user=user)
        if Transaction.objects.filter(card=card).count() + len(cart) >= MAX_COPIES_PER_CARD:
            return True
        return False

    def still_borrow_overdue_books(self, user): 
        return False

    def make_sure_cart_in_session(self, request):
        if 'cart' not in request.session:
            request.session['cart'] = []


@method_decorator([csrf_exempt, login_required], name='dispatch')
class AddToCartView(CartView):

    def get(self, request):
        add_to_cart_form = AddToCartForm()
        self.make_sure_cart_in_session(request)
        books = Book.objects.filter(id__in=[item['book_id'] for item in request.session['cart']])
        context = {'user': request.user, 'add_to_cart_form': add_to_cart_form, 'books': books}
        return TemplateResponse(request, self.template_name, context)

    def post(self, request):
        add_to_cart_form = AddToCartForm(request.POST)

        if add_to_cart_form.is_valid():
            self.make_sure_cart_in_session(request)
            cart = request.session['cart']
            book = add_to_cart_form.cleaned_data['book_id']

            # Stage 1:
            if not self.has_card(request.user):
                context = self.get_context(request, 'You do not have borrowing card')
                return TemplateResponse(request, self.template_name, context)
            # Stage 2:
            if self.card_expired(request.user):
                context = self.get_context(request, 'Your card is expired')
                return TemplateResponse(request, self.template_name, context)
            # Stage 3:
            if self.in_possession(book, request):
                context = self.get_context(request, 'You already possess this book')
                return TemplateResponse(request, self.template_name, context)
            # Stage 4:
            if self.still_borrow_overdue_books(request.user):
                context = self.get_context(request, 'You still must return overdue books to borrow more')
                return TemplateResponse(request, self.template_name, context)
            # Stage 5:
            if self.exceed_max_copies(request.user, cart):
                context = self.get_context(request, 'Cart is full')
                return TemplateResponse(request, self.template_name, context)
            # Okay, now save to cart
            cart.append(book)
            request.session['cart'] = cart
            books = Book.objects.filter(id__in=[item['book_id'] for item in cart])
            context = {'user': request.user, 'add_to_cart_form': add_to_cart_form, 'books': books}
            return TemplateResponse(request, self.template_name, context)

        context = {'user': request.user, 'add_to_cart_form': add_to_cart_form}        
        return TemplateResponse(request, self.template_name, context)


@method_decorator([csrf_exempt, login_required], name='dispatch')
class ClearCartView(View):
    template_name = '%s/cart.html' % APPNAME

    def get(self, request):
        request.session['cart'] = []
        context = {'user':request.user}
        return TemplateResponse(request, self.template_name, context)
    
    def post(self, request):
        request.session['cart'] = []
        context = {'user':request.user}
        return TemplateResponse(request, self.template_name, context)


@method_decorator([csrf_exempt, login_required], name='dispatch')
class CheckOutView(CartView):

    def get(self, request):
        self.make_sure_cart_in_session(request)
        cart = request.session['cart']

        while len(cart) > 0:
            item = cart.pop(0)
            
            if not Book.objects.filter(id=item['book_id']).exists():
                self.get_context(request, 'Book with id %d is not exist anymore' % item['book_id'])
                return TemplateResponse(request, self.template_name, context)
            if not Copy.objects.filter(id=item['copy_id']).exists():
                self.get_context(request, 'Copy with id %d is not exist anymore' % item['copy_id'])
                return TemplateResponse(request, self.template_name, context)
            copy = Copy.objects.get(id=item['copy_id'])
            if copy.type != 'A':
                self.get_context(request, 'Copy with id %d is not available anymore' % item['copy_id'])
                return TemplateResponse(request, self.template_name, context)
            if Transaction.objects.filter(copy=copy).exists():
                self.get_context(request, 'Copy with id %d is not available anymore' % item['copy_id'])
                return TemplateResponse(request, self.template_name, context)

            Transaction.objects.create(
                type='B',
                start=date.today(),
                card=Card.objects.get(user=request.user),
                copy=copy,
            )

        request.session['cart'] = cart
        context = self.get_context(request)
        return TemplateResponse(request, self.template_name, context)


@method_decorator([csrf_exempt, login_required], name='dispatch')
class BorrowView(View):

    template_name = '%s/borrow.html' % APPNAME

    def get(self, request):
        try:
            card = Card.objects.get(user=request.user)
        except Card.DoesNotExist:
            context = {'user': request.user}
            return TemplateResponse(request, self.template_name, context)

        borrowed = []
        for transaction in Transaction.objects.filter(card=card):
            borrowed.append({
                'transaction': transaction,
                'copy': transaction.copy,
                'book': transaction.copy.book,    
            })
        context = {'user': request.user, 'borrowed': borrowed}
        return TemplateResponse(request, self.template_name, context)


@method_decorator([csrf_exempt, login_required], name='dispatch')
class LentView(View):
    
    template_name = '%s/lent.html' % APPNAME

    def get(self, request):
        if 'lent_username' in request.session:
            del request.session['lent_username']
        context = {
            'user': request.user,
            'search_borrowing_information_form':  SearchBorrowInformationForm(),
        }
        return TemplateResponse(request, self.template_name, context)

    def post_stage_1_show_borrowing_information(self, request):
        search_borrowing_information_form = SearchBorrowInformationForm(data=request.POST)

        if search_borrowing_information_form.is_valid():
            username = search_borrowing_information_form.cleaned_data['username']
            user = User.objects.get(username=username)
            card = Card.objects.get(user=user)
            transactions = Transaction.objects.filter(card=card, type='B')
            lent_form = LentForm(transactions)
            context = {'user': request.user, 'transactions': transactions, 'lent_form': lent_form}
            request.session['lent_username'] = username
            return TemplateResponse(request, self.template_name, context)

        context = {
            'user': request.user, 
            'search_borrowing_information_form': search_borrowing_information_form,
        }
        return TemplateResponse(request, self.template_name, context)

    def post_stage_2_record_lent_information(self, request):
        user = User.objects.get(username=request.session['lent_username'])
        card = Card.objects.get(user=user)
        transactions = Transaction.objects.filter(card=card, type='B')
        lent_form = LentForm(transactions, data=request.POST)

        if lent_form.is_valid():
            for transaction_id in lent_form.cleaned_data['transactions']:
                
                transaction = Transaction.objects.get(id=transaction_id)
                history = History.objects.create(
                    type='B',
                    start=transaction.start,
                    card=transaction.card,
                    copy=transaction.copy, 
                    description='borrowed copy lent to user',
                )
                history.save()
                transaction.delete()
                new_transaction = Transaction.objects.create(
                    type='L',
                    card=transaction.card,
                    copy=transaction.copy,
                )
                new_transaction.save()
            del request.session['lent_username']

        context = {
            'user': request.user, 
            'lent_form': lent_form,
        }
        return TemplateResponse(request, self.template_name, context)

    def post(self, request):
        if 'lent_username' in request.session:
            return self.post_stage_2_record_lent_information(request)
        return self.post_stage_1_show_borrowing_information(request)


@method_decorator([csrf_exempt, login_required], name='dispatch')
class HistoryView(View):
    template_name = '%s/history.html' % APPNAME

    def get(self, request):
        card = Card.objects.get(user=request.user) # A little bit sloppy here
                                                   # But I have no time TT.TT
        records = []
        for history in History.objects.filter(card=card):
            records.append({
                'history': history,
                'card': history.copy,
                'book': history.copy.book,    
            })                                               
        context = {
            'user': request.user, 
            'records': records,
        }
        return TemplateResponse(request, self.template_name, context)


@method_decorator([csrf_exempt, login_required], name='dispatch')
class ReturnView(View):

    template_name = '%s/return.html' % APPNAME

    def get(self, request):
        if 'lent_information' in request.session:
            del request.session['lent_information']
        context = {
            'user': request.user,
            'search_lent_information_form': SearchLentInformationForm(),
        }
        return TemplateResponse(request, self.template_name, context)
        
    def post_stage_1_search_lent_information(self, request):
        search_lent_information_form = SearchLentInformationForm(request.POST)
        if search_lent_information_form.is_valid():
            card_id = search_lent_information_form.cleaned_data['card_id']
            sequence_number = search_lent_information_form.cleaned_data['sequence_number']

            if card_id:
                card = Card.objects.get(id=card_id)
                transactions = Transaction.objects.filter(card=card)
                transaction_ids = [transaction.id for transaction in transactions]
                context = {
                    'user': request.user,
                    'verify_lent_information_forms' : [{
                        'transaction_id': transaction_id,
                        'form': VerifyLentInformationForm(
                            initial={'transaction_id':transaction_id}
                            )} for transaction_id in transaction_ids
                        ],
                }
                request.session['lent_information'] = search_lent_information_form.cleaned_data
                return TemplateResponse(request, self.template_name, context)

            if sequence_number:
                copy = Copy.objects.get(sequence_number=sequence_number)
                transactions = Transaction.objects.filter(copy=copy)
                transaction_ids = [transaction.id for transaction in transactions]
                context = {
                    'user': request.user,
                    'verify_lent_information_forms' : [{
                        'transaction_id': transaction_id,
                        'form': VerifyLentInformationForm(
                            initial={'transaction_id':transaction_id}
                            )} for transaction_id in transaction_ids
                        ],
                }
                request.session['lent_information'] = search_lent_information_form.cleaned_data
                return TemplateResponse(request, self.template_name, context)
                
        context = {
            'user': request.user,
            'search_lent_information_form': search_lent_information_form,
        }
        return TemplateResponse(request, self.template_name, context)

    def post_stage_2_verify_lent_information(self, request):
        verify_lent_information_form = VerifyLentInformationForm(request.POST)
        if verify_lent_information_form.is_valid():
            context = {
                'user': request.user,
                'compensation_form': CompensationForm(),
            }
            lent_information = request.session['lent_information']
            lent_information['transaction_id'] = verify_lent_information_form.cleaned_data['transaction_id']
            request.session['lent_information'] = lent_information
            return TemplateResponse(request, self.template_name, context)

        context = {
            'user': request.user,
            'search_lent_information_forms': [
                {'transaction_id': verify_lent_information_form.cleaned_data['transaction_id'],
                'form': verify_lent_information_form}
            ]
        }
        return TemplateResponse(request, self.template_name, context)

    def post_stage_3_enter_compensation_and_verify(self, request):
        print 'stage 3'
        request.POST = request.POST.copy()
        request.POST['transaction_id'] = request.session['lent_information']['transaction_id']
        compensation_form = CompensationForm(request.POST)
        if compensation_form.is_valid():
            print 'stage 3 form valid'
            transaction = Transaction.objects.get(id=request.session['transaction_id'])

            history = History.objects.create(
                type='L',
                start=transaction.start,
                card=transaction.card,
                copy=transaction.copy,
                description='returned to library with compensation %d VND' % 
                    compensation_form.cleaned_data['compensation_money']
            )
            history.save()
            transaction.delete()
            context = {
                'user': request.user,
                'message': 'Success'
            }
            del request.session['lent_information']
            return TemplateResponse(request, self.template_name, context)

        print 'stage 3 form invalid'
        context = {
            'user': request.user,
            'compensation_form': compensation_form,
        }

        return TemplateResponse(request, self.template_name, context)

    def post(self, request):
        if 'lent_information' not in request.session:
            return self.post_stage_1_search_lent_information(request)
        if 'transaction_id' not in request.session['lent_information']:
            return self.post_stage_2_verify_lent_information(request)
        return self.post_stage_3_enter_compensation_and_verify(request)

