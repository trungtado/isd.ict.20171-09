from django.conf.urls import url
from django.contrib.auth import views as auth_views
from .views import *

urlpatterns = [
    url(r'^add_to_cart/$', AddToCartView.as_view(), name='add_to_cart'),
    url(r'^clear_cart/$', ClearCartView.as_view(), name='clear_cart'),
    url(r'^cart/$', CartView.as_view(), name='cart'),
    url(r'^check_out/$', CheckOutView.as_view(), name='check_out'),
    url(r'^borrow/$', BorrowView.as_view(), name='borrow'),
    url(r'^lent/$', LentView.as_view(), name='lent'),
    url(r'^history/$', HistoryView.as_view(), name='history'),
    url(r'^return/$', ReturnView.as_view(), name='return'),
]
