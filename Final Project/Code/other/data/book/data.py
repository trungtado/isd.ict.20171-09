import time
import json
import sqlite3
import requests
from pprint import pprint

conn = sqlite3.connect('../../../db.sqlite3')
cur = conn.cursor()
url = 'http://localhost:8000/book/backdoor/'


def get_subclassification_id(subclassification):
    c_label = subclassification[0]
    s_label = subclassification[1:]
    cur.execute('''SELECT s.id 
        FROM book_classification AS c, book_subclassification as s
        WHERE s.classification_id=c.id
        AND c.label=?
        AND s.label=?''', (c_label, s_label))
    return cur.fetchone()[0]


def get_cover(url):
    r = requests.get(url)
    with open('tmp.jpeg', 'wb') as fp:
        fp.write(r.content)


def process_book(book):
    try:
        book['subclassification_id'] = get_subclassification_id(book['subclassification'])
    except:
        print 'bug', book['subclassification']
        raise
    get_cover(book['cover'])
    pprint(book)
    r = requests.post(url,
        data=book,
        files={'cover':open('tmp.jpeg', 'rb')}
    )
    if 'Successfully registered' not in r.content:
        print r.content


def main():
    books = json.loads(open('data.json').read())
    for book in books:
        for i in range(10):
            try:
                process_book(book)
            except:
                time.sleep(1)
            else:
                break

if __name__ == '__main__':
    main()