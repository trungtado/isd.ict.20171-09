import re
import sqlite3

data = open('data.txt').read()
conn = sqlite3.connect('../../../db.sqlite3')
cur = conn.cursor()

subclasses = re.findall(r'Subclass\s+([^\n]+)', data)
cur.execute('DELETE FROM book_subclassification')

for subclass in subclasses:
    class_label = subclass[0]
    cur.execute('SELECT id FROM book_classification WHERE label=?', class_label)
    class_id = cur.fetchone()[0]
    try:
        label = re.findall(r'([^\s+]+)\s+-\s+', subclass[1:])[0]
    except:
        label = '-'
    description = re.findall(r'-\s+(.+)$', subclass)[0]
    print class_id, label, description
    cur.execute('INSERT INTO book_subclassification (label, description, classification_id) VALUES (?, ?, ?)', (label, description, class_id))

conn.commit()