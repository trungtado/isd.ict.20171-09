import re
import sqlite3

data = open('data.txt').read()
conn = sqlite3.connect('../../../db.sqlite3')
cur = conn.cursor()

classes = re.findall(r'Class\s+([^\n]+)', data)

cur.execute('DELETE FROM book_classification')

for class_ in classes:
    if '--' in class_:
        continue
    label = class_[0]
    description = re.findall(r'-\s+(.+)$', class_)[0]
    print label, description
    cur.execute('INSERT INTO book_classification (label, description) VALUES (?, ?)', (label, description))

conn.commit()