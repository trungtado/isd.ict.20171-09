# -*- coding:utf-8 -*- #
from .forms import *
from .models import *
from pprint import pprint 
from config import settings
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.shortcuts import render
from django.template.response import TemplateResponse
from django.views.generic.detail import DetailView as TemplateDetailView
from django.views import View
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.utils.http import is_safe_url
from django.shortcuts import resolve_url
from django.contrib.auth import login as auth_login
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

APPNAME = 'book'

@method_decorator([csrf_exempt, login_required], name='dispatch')
class RegisterView(View):

    template_name = '%s/book.html' % APPNAME

    def get(self, request):
        if 'register_book_info' in request.session:
            del request.session['register_book_info']
        context = {'isbn_form': IsbnForm(), 'user': request.user}
        return TemplateResponse(request, self.template_name, context)

    def post_stage_1_check_isbn(self, request):
        isbn_form = IsbnForm(request.POST)

        if isbn_form.is_valid():
            register_book_info = {
                'isbn': isbn_form.cleaned_data['isbn'],
                'existed': self.isbn_existed(isbn_form.cleaned_data['isbn'])
            }
            context = {
                'register_book_info': register_book_info, 
                'user': request.user
            }
            if register_book_info['existed']:
                context['register_copy_form'] = RegisterCopyForm()
                context['book'] = Book.objects.get(isbn=register_book_info['isbn'])
            else:
                context['register_book_form'] = RegisterBookForm()
            request.session['register_book_info'] = register_book_info
            return TemplateResponse(request, self.template_name, context)

        context = {'isbn_form': isbn_form, 'user': request.user}
        return TemplateResponse(request, self.template_name, context)

    def post_stage_2_flow_1_register_book(self, request):
        register_book_form = RegisterBookForm(request.POST)

        if register_book_form.is_valid():
            register_book_form.save()
            request.session['register_book_info']['existed'] = True
            context = {
                'message': 'Successfully registered book, now register copies of that book',
                'register_copy_form': RegisterCopyForm(),
                'user': request.user,
                'book': Book.objects.get(isbn=request.session['register_book_info']['isbn']),
            }
            return TemplateResponse(request, self.template_name, context)

        context = {'register_book_form': register_book_form, 'user': request.user}
        return TemplateResponse(request, self.template_name, context)

    def post_stage_2_flow_2_register_copy(self, request):
        request.POST = request.POST.copy()
        request.POST['isbn'] = request.session['register_book_info']['isbn']
        register_copy_form = RegisterCopyForm(request.POST)

        if register_copy_form.is_valid():
            register_copy_form.save()
            context = {
                'message': 'Successfully registered copies, now register more copies.',
                'register_copy_form': RegisterCopyForm(),
                'user': request.user,            
                'book': Book.objects.get(isbn=request.session['register_book_info']['isbn']),
            }
            return TemplateResponse(request, self.template_name, context)

        context = {'register_copy_form': register_copy_form, 'user': request.user}
        return TemplateResponse(request, self.template_name, context)

    def post(self, request):
        if 'register_book_info' in request.session:
            if request.session['register_book_info']['existed']:
                return self.post_stage_2_flow_2_register_copy(request)
            return self.post_stage_2_flow_1_register_book(request)
        return self.post_stage_1_check_isbn(request)

    def isbn_existed(self, isbn):
        if Book.objects.filter(isbn=isbn).exists():
            return True
        return False


@method_decorator([csrf_exempt, login_required], name='dispatch')
class UserView(View):
    
    template_name = '%s/book.html' % APPNAME
    
    def get(self, request):
        context = {'books': Book.objects.all(), 'user': request.user}
        return TemplateResponse(request, self.template_name, context)
        

@method_decorator([csrf_exempt], name='dispatch')
class BackdoorRegisterView(View):

    template_name = '%s/book.html' % APPNAME

    def get(self, request):
        context = {'register_book_form':RegisterBookForm()}
        return TemplateResponse(request, self.template_name, context)
        
    def post(self, request):

        register_book_form = RegisterBookForm(request.POST, request.FILES)

        if register_book_form.is_valid():
            register_book_form.save()
            context = {'register_book_form': RegisterBookForm(), 'message':'Successfully registered'}
            return TemplateResponse(request, self.template_name, context)

        context = {'register_book_form': register_book_form}
        return TemplateResponse(request, self.template_name, context)


@method_decorator([csrf_exempt], name='dispatch')
class SearchView(View):

    template_name = '%s/book.html' % APPNAME

    def get(self, request):
        context = {'search_book_form':SearchBookForm(), 'user': request.user}
        return TemplateResponse(request, self.template_name, context)

    def post(self, request):
        search_book_form = SearchBookForm(request.POST)
        context = {'search_book_form':search_book_form, 'user': request.user}
        if search_book_form.is_valid():
            context['books'] = Book.objects.all()
            if search_book_form.cleaned_data['title']: 
                context['books'] = context['books'].filter(
                    title__contains=search_book_form.cleaned_data['title'])
            if int(search_book_form.cleaned_data['classification_id']): 
                context['books'] = context['books'].filter(
                    subclassification__classification_id=search_book_form.cleaned_data['classification_id'])
            if search_book_form.cleaned_data['authors']: 
                context['books'] = context['books'].filter(
                    authors__contains=search_book_form.cleaned_data['authors'])
            if search_book_form.cleaned_data['publisher']: 
                context['books'] = context['books'].filter(
                    publisher__contains=search_book_form.cleaned_data['publisher'])
            return TemplateResponse(request, self.template_name, context)

        return TemplateResponse(request, self.template_name, context)


@method_decorator([csrf_exempt], name='dispatch')
class DetailView(TemplateDetailView):

    model = Book
    template_name = '%s/book.html' % APPNAME
    
    def get_context_data(self, **kwargs):
        context = super(DetailView, self).get_context_data(**kwargs)
        book = kwargs['object']
        context['book'] = book
        context['subclassification'] = book.subclassification
        context['classification'] = book.subclassification.classification
        return context


@method_decorator([csrf_exempt, login_required], name='dispatch')
class CopyView(View):

    template_name = '%s/copy.html' % APPNAME

    def get(self, request):
        books = []
        for book in Book.objects.all():
            count = Copy.objects.filter(book=book).count() 
            if count:
                books.append({'book': book, 'count': count})
        context = {
            'user': request.user,
            'books': books
        }
        return TemplateResponse(request, self.template_name, context)
