from string import digits
from django import forms
from django.db import models
from django.core.exceptions import ValidationError
from .models import *


class IsbnForm(forms.Form):
    isbn = forms.CharField()

    def clean_isbn(self):
        isbn = self.cleaned_data['isbn']
        for c in isbn:
            if c not in digits:
                raise forms.ValidationError('ISBN must contain only digits')
        return isbn


class RegisterBookForm(forms.ModelForm):

    try:
        CHOICES = tuple(
            [(item.id, item.classification.label + item.label \
            + ' ' + item.classification.description + ' ' + item.description) \
            for item in Subclassification.objects.all()]
        )
    except:
        CHOICES = ()
    subclassification_id = forms.ChoiceField(choices=CHOICES)
    sequential_number = forms.IntegerField(widget=forms.HiddenInput(), required=False)

    class Meta:
        model = Book
        fields = ('title', 'publisher', 'authors', 'isbn', 'cover', 'description')

    def clean_isbn(self):
        isbn = self.cleaned_data['isbn']
        for c in isbn:
            if c not in digits:
                raise forms.ValidationError('ISBN must contain only digits')
        return isbn

    def save(self):
        self.cleaned_data['sequential_number'] = Book.objects.filter(
            subclassification_id=self.cleaned_data['subclassification_id']).\
            aggregate(models.Max('sequential_number'))['sequential_number__max']
        if self.cleaned_data['sequential_number']:
            self.cleaned_data['sequential_number'] += 1
        else:
            self.cleaned_data['sequential_number'] = 1
        book = Book.objects.create(**self.cleaned_data)
        book.save()
        return book


class RegisterCopyForm(forms.ModelForm):

    isbn = forms.CharField(widget=forms.HiddenInput(), required=False)

    class Meta:
        model = Copy
        fields = ('type', 'price')

    def save(self):
        book = Book.objects.get(isbn=self.cleaned_data['isbn'])
        self.cleaned_data['book'] = book

        self.cleaned_data['sequential_number'] = Copy.objects.filter(book=book).\
            aggregate(models.Max('sequential_number'))['sequential_number__max']
        if self.cleaned_data['sequential_number']:
            self.cleaned_data['sequential_number'] += 1
        else:
            self.cleaned_data['sequential_number'] = 1
        del self.cleaned_data['isbn']
        copy = Copy.objects.create(**self.cleaned_data)
        copy.save()
        return copy
        


class SearchBookForm(forms.Form):

    try:
        CHOICES = [(item.id, item.label + ' ' + item.description) for item in Classification.objects.all()]
    except:
        CHOICES = []
    CHOICES = tuple([(0, None)] + CHOICES)

    title = forms.CharField(required=False)
    classification_id = forms.ChoiceField(choices=CHOICES, required=False)
    authors = forms.CharField(required=False)
    publisher = forms.CharField(required=False)
