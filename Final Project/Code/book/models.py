import os
import string
import random
from django.db import models

def randbytes(k=10):
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(k))

def rand_img_path(instance, filename):
    ls = filename.split('.')
    postfix = ls[len(ls)-1]
    basename = randbytes() + '.' + postfix
    path = os.path.join('media/book_cover', basename)
    while os.path.exists(path):
        basename = randbytes()
        path = os.path.join('media/book_cover', basename)
    return os.path.join('book_cover', basename)


class Classification(models.Model):

    label = models.CharField(max_length=1)
    description = models.TextField()

    class Meta:
        unique_together = ('label', )


class Subclassification(models.Model):
    
    label = models.CharField(max_length=5)
    description = models.TextField()
    classification = models.ForeignKey(Classification, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('label', 'classification')


class Book(models.Model):

    sequential_number = models.IntegerField()
    subclassification = models.ForeignKey(Subclassification)
    title = models.CharField(max_length=300)
    publisher = models.CharField(max_length=300)
    authors = models.CharField(max_length=300)
    isbn = models.CharField(max_length=30, unique=True)
    description = models.TextField()
    cover = models.ImageField(upload_to=rand_img_path, blank=True)

    class Meta:
        unique_together = ('sequential_number', 'subclassification')


class Copy(models.Model):

    TYPES = (
        ('R', 'Reference'), 
        ('A', 'Borrowable'),
    )
    sequential_number = models.IntegerField()
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    type = models.CharField(max_length=1, choices=TYPES)
    price = models.FloatField()
    
    class Meta:
        unique_together = ('sequential_number', 'book')