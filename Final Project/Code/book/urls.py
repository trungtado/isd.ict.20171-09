from django.conf.urls import url
from django.contrib.auth import views as auth_views
from .views import *

urlpatterns = [
    url(r'^register/$', RegisterView.as_view(), name='register'),
    url(r'^backdoor/$', BackdoorRegisterView.as_view()),
    url(r'^search/$', SearchView.as_view(), name='search'),
    url(r'^detail/(?P<pk>\d+)/$', DetailView.as_view(), name='detail'),
    url(r'^copy/$', CopyView.as_view(), name='copy'),
]
