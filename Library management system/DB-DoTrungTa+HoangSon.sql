-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 25, 2017 lúc 06:10 AM
-- Phiên bản máy phục vụ: 10.1.28-MariaDB
-- Phiên bản PHP: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `group-9`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `book`
--

CREATE TABLE `book` (
  `bookNumber` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `areasOfInteres` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `publisher` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `authors` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ISBN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `book`
--

INSERT INTO `book` (`bookNumber`, `areasOfInteres`, `title`, `publisher`, `authors`, `ISBN`) VALUES
('1', 'asdasd', 'dfdfefs', 'asdfds', 'dfgdf', 'asfasd'),
('2', 'afaasf', 'SDFAG', 'SAFAGF', 'HGDSSDF', 'GAFAEFS'),
('3', 'sdfgfhg', 'sdfgsbvc', 'sdfgsf', 'asdfgh', 'asdfgbn '),
('4', 'sdfgh', 'sdfghgfd', 'sdfg', 'dsfgs', 'sdfgfd');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `borrowercard`
--

CREATE TABLE `borrowercard` (
  `borrowerNumber` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `userName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expiredDate` date NOT NULL,
  `activatedCode` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `borrowercard`
--

INSERT INTO `borrowercard` (`borrowerNumber`, `userName`, `expiredDate`, `activatedCode`) VALUES
('1', 'A', '2017-10-10', 'dasfadasda'),
('2', 'B', '2017-10-18', 'avdsdadasd'),
('3', 'C', '2017-10-03', 'dfdsdasda'),
('4', 'D', '2017-10-27', 'afadasdas');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `borrowinginfo`
--

CREATE TABLE `borrowinginfo` (
  `borrowerNumber` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `userName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bookNumber` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `copyNumber` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `BorrowedDate` date NOT NULL,
  `lentDate` date NOT NULL,
  `expectedReturnDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `borrowinginfo`
--

INSERT INTO `borrowinginfo` (`borrowerNumber`, `userName`, `bookNumber`, `copyNumber`, `BorrowedDate`, `lentDate`, `expectedReturnDate`) VALUES
('1', 'A', '1', '2', '2017-10-01', '2017-10-02', '2017-10-04'),
('2', 'B', '2', '4', '2017-10-10', '2017-10-12', '2017-10-26'),
('3', 'C', '3', '5', '2017-10-02', '2017-10-04', '2017-10-06'),
('4', 'D', '4', '3', '2017-10-04', '2017-10-13', '2017-10-26');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `copy`
--

CREATE TABLE `copy` (
  `bookNumber` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `copyNumber` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pricce` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `copy`
--

INSERT INTO `copy` (`bookNumber`, `copyNumber`, `type`, `pricce`) VALUES
('1', '2', 'novel', '23424'),
('2', '3', 'novel', '345364'),
('3', '7', 'novel', '4535'),
('4', '6', 'novel', '345354');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `huststudent`
--

CREATE TABLE `huststudent` (
  `userName` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `studentID` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `period` int(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `huststudent`
--

INSERT INTO `huststudent` (`userName`, `studentID`, `period`) VALUES
('A', '11111', 1231),
('B', '3123', 4234),
('C', '3432', 7567),
('D', '4563', 5345);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user`
--

CREATE TABLE `user` (
  `userName` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `fullName` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `contact` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `user`
--

INSERT INTO `user` (`userName`, `password`, `fullName`, `email`, `gender`, `contact`) VALUES
('A', 'afsadsa', 'AAA', 'asdas@gmail.com', 'male', 'asasdasd'),
('B', 'afgsdgfds', 'BBB', 'ASDASD@gmail.com', 'male', 'asdadasd'),
('C', 'asfdgfdgg', 'CCC', 'adsfd@hmail.com', 'female', 'asdfasdas'),
('D', 'asadasdas', 'DDD', 'asdas@gmail.com', 'female', 'asdsdfsd');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`bookNumber`);

--
-- Chỉ mục cho bảng `borrowercard`
--
ALTER TABLE `borrowercard`
  ADD PRIMARY KEY (`borrowerNumber`,`userName`);

--
-- Chỉ mục cho bảng `borrowinginfo`
--
ALTER TABLE `borrowinginfo`
  ADD PRIMARY KEY (`borrowerNumber`,`userName`,`bookNumber`,`copyNumber`,`BorrowedDate`);

--
-- Chỉ mục cho bảng `copy`
--
ALTER TABLE `copy`
  ADD PRIMARY KEY (`bookNumber`,`copyNumber`),
  ADD KEY `bookNumber` (`bookNumber`);

--
-- Chỉ mục cho bảng `huststudent`
--
ALTER TABLE `huststudent`
  ADD PRIMARY KEY (`userName`);

--
-- Chỉ mục cho bảng `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userName`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
