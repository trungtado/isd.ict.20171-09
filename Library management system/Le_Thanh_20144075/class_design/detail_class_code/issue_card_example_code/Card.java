import java.util.Date;

private class Card {
    /** The borrower unique identification number**/
    private int number;
    /** The due date which borrower cannot use this card anymore **/
    private Date expiredDate;
    /** The code for borrower to activate this card **/
    private int activationCode;
    /** If this attribute is False, borrower cannot use this card **/
    private boolean isActivated;

    /**
    * Create a new card by random initialize number, activationCode
    * and set expiredDate equal to today plus 6 month, activated is
    * False
    **/
    public void Card(){
    }

    public int getNumber(){
    }

    public Date getExpiredDate(){
    }

    public int getActivationCode(){
    }

    public boolean getIsActivated(){
    }
}