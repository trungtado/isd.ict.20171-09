private class CardController {
    /**
    * Create new Borrower object with given attribute in argument
    * if isStudent or isDeposited satisfied then create new Card
    * object, then set the card to Borrower object, save Borrower
    * object to the database.
    * @return A {@link Card} object
    * @param A {@link boolean} isStudent is True if the borrower is student of the 
    * university
    * @param A {@link boolean} isDeposited is True if the borrower paid the deposit
    * money
    * @see {@link Borrower}
    * @see {@link Card}
    **/
    public Card issueNewCard(boolean isStudent, boolean isDeposited){
    }

}