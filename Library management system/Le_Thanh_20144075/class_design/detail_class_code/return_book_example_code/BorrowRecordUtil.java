class BorrowRecordUtil {

    /**
    * Get list of {@link BorrowRecord} object
    * @param A {@link Borrower} object
    * @return An Array of {@link BorrowRecord}
    **/
    public BorrowRecord getRecords(Borrower borrower){
    }

    /**
    * Delete instance of {@link BorrowRecord} object in database
    * @return A {@link BorrowRecord} object
    **/
    public void deleteRecord(BorrowRecord record){
    }

    /**
    * Delete instance of {@link BorrowRecord} object in database
    * @param A borrower instance
    * @param An amount {@link int} of money
    **/
    public  void cumulateCompensateMoney(Borrower borrower, int amount){
    }
}