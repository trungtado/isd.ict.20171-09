import pymysql

conn = pymysql.connect('localhost', 'root', 'password', 'dbname')
cur = conn.cursor()

# Card(id, expired, activation_code, is_activated)
query = '''CREATE TABLE card (
    id INT(6) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    expired DATE,
    activation_code VARCHAR(6),
    is_activated TINYINT(1)
);
'''
cur.execute(query)
conn.commit()

# User(id, username, password)
query = '''CREATE TABLE user(
    id INT(6) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    username VARCHAR(20),
    password VARCHAR(200)
);
'''
cur.execute(query)
conn.commit()

# Borrower(id, user_id, c_money, is_student, is_deposited)
query = '''CREATE TABLE borrower (
    id INT(6),
    user_id INT(6),
    c_money INT(10),
    is_student TINYINT(1),
    is_deposited TINYINT(1),
    CONSTRAINT FOREIGN KEY (user_id) REFERENCES user(id) ON DELETE CASCADE ON UPDATE CASCADE
);
'''
cur.execute(query)
conn.commit()

# Book(id, ibsn, title)
query = '''CREATE TABLE book (
    id INT(6) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    ibsn VARCHAR(20),
    title VARCHAR(200)
);
'''
cur.execute(query)
conn.commit()

# BorrowerBook(borrower_id, book_id, amount)
query = '''CREATE TABLE borrower_book (
    borrower_id int(6),
    book_id int(6),
    amount int(3),
);
'''
cur.execute(query)
conn.commit()


# Index on borrower_id because "Return book" usecase always find borrow record 
# by borrower_id. Index on borrower_id make that look up action more efficient
query = '''CREATE INDEX borrower_id_index ON borrower_book(borrower_id) USING BTREE;'''
