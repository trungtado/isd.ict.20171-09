/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RegisterToBorrow.Controller;

import RegisterToBorrow.Model.BookInfo;
import RegisterToBorrow.Model.BorrowerCard;
import RegisterToBorrow.Model.Copy;

/**
 *
 * @author Mosquito
 */
public class RegisterBorrowBookController {
    private String bookID;
    private int duration;
    private int numberOfCopy;
    private BorrowerCard borrower;
/**
 * control the flow of the register to borrow book
 * @param bookID The ID of the book the user want to borrow
 * @param duration The amount of time the user want to borrow 
 * @param numberOfCopy The number of book the user want to borrow
 */    
    public void borrowBook(String bookID,int duration, int numberOfCopy){
        
    }
    /**
     * check if the current user is able to register to borrow book
     * @param borrowerCardNumber the user card number
     * @return if the user is valid or not
     */
    private boolean checkBorrowerCard(int borrowerCardNumber){
        return false;
    }
    /**
     * Check if the user card is expired
     * @param user the borrower card we are checking
     * @return if the borrower card we are checking is expired or not
     */
    private boolean checkExpired(BorrowerCard user){
        return false;
    }
    /**
     * Check if the user card has any unreturned book
     * @param user the borrower card we are checking
     * @return if the user we are checking has any unreturned book or not
     */
    private boolean checkUnreturnedBook(BorrowerCard user){
        return false;
    }
    /**
     * Check if the copy of the book we are checking is available to borrow
     * @param book the book we are checking
     * @return if the book is available or not
     */
    private boolean checkCopyInformation(BookInfo book){
        return false;
    }
    /**
     * Check if the status of the copy of the book we are checking
     * @param copy the copy we are checking
     * @return if the copy is available or not
     */
    private boolean checkStatus(Copy copy ){
        return false;
    }
    /**
     * After confirm to borrow update the copy info of the book
     * @param copy the copy we need to update
     */
    private void updateCopyInfo(Copy copy){
        
    }
    /**
     * Check if in 2 days from the date the copy the user take the book
     * @return if the user take the book
     */
    private boolean checkCollect(){
        return false;
    }
}
