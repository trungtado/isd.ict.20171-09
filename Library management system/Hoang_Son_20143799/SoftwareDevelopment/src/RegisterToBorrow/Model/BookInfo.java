/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RegisterToBorrow.Model;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Mosquito
 */
public class BookInfo {
    private String bookID;
    private String bookTitle;
    private String description;
    private String author;
    private String publisher;
    private Date releaseDate;
    private List<Copy> copies;
    /**
     * get the information of the book we are checking
     * @param bookID the book we need to find
     * @return the book with the needed information
     */
    public BookInfo getBookInfo(String bookID ){
        return null;   
    }
}
