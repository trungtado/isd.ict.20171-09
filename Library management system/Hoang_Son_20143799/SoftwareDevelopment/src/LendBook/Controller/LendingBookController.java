/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LendBook.Controller;

import LendBook.Model.BorrowingInfo;

/**
 *
 * @author Mosquito
 */
public class LendingBookController {
    /**
     * Find the borrowing information 
     * @param searchInput the string the user input
     */
    public void findBorrowingInfo(String searchInput){
        
    }
    
    /**
     * Change page to display the detail borrowing information
     * @param borrow the borrowing we need to display
     */
    public void viewDetail(BorrowingInfo borrow){
        
    }
    /**
     * When user click confirm
     */
    public void inputConfirmation (){
    
    }
    /**
     * update the borrowing information
     * @param borrowing the borrowing we need to update
     */
    private void updateBorrowing( BorrowingInfo borrowing){
        
    }
}
